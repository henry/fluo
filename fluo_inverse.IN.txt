&geometrieXYZ ! concerne aussi la psf? voir avec Kevin
lx=10 ly=10 lz=1  !! dimension du domaine en nm
Nx=64 Ny=64 	!! decoupage du plan xy
Nz=1           !! nombre de plans en Z
/
&illuminations 
n_illum=1 
/
&fichiers 
write_to_files=.true.
! densite="/home/me/AMUbox/projets/fluo/build/gauss_f_2d.mat"
densite="/home/me/AMUbox/projets/fluo/build/densite_25.txt"
illumination="illumination.txt"
psf="../../datas/psf3d.txt"
measures="../../datas/cell_uniforme.txt"
/
&iterations
nb_iter=1
tol=1e-6
/
&capteur  ! le capteur utilise par Kevin
Nxc=1392
Nyc=1040
/
&fft  ! paramètres de la FFT
Nxfft=128
Nyfft=128
/
&choixalgo  ! choix du type de PSF suivant algo: Gaussienne, Bessel
algo_psf="Gaussienne"
/
