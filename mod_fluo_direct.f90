! Created by me on 01/06/2021.

module mod_fluo_direct
  use mod_params
  use mod_utils
  use FFTW3
  use mod_fft

  implicit none

contains

  subroutine direct(i_ill, fft_psf)
    ! calcul de M = \rho I * p
    ! entrées:
    !     \rho: params_direct%rho
    !     I: params_direct%ill : illumination
    !     p: psf, mais on a déjà la fft de la psf: fft_psf
    ! sorties:
    !     M: résultat dans params_direct%data

    implicit none

    integer, intent(in) :: i_ill
    double complex, dimension(:,:,:), allocatable, intent(in)   :: fft_psf
    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: h   ! resultat du produit de convolution, support double

    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: ztemp, ztemp2, h2
    integer :: ix, iy, iz, nx, ny, nz
    integer :: nxfft, nyfft
    integer(8)       :: plan2f, plan2b
    integer          :: iret, omp_get_max_threads ! pour FFT en

    nx      = params_direct%Nx
    ny      = params_direct%Ny
    nz      = params_direct%Nz

    nxfft = params_fft%Nxfft
    nyfft = params_fft%Nyfft

    allocate( h( nx, ny ))
    allocate( h2( nxfft, nyfft ))
    h2 = 0.0d0

    allocate( ztemp(  nxfft, nyfft )  )
    ztemp = 0.0d0
    allocate( ztemp2(  nxfft, nyfft )  )
    ztemp2 = 0.0d0

#ifdef DEBUG
    print*, __FILE__, __LINE__, " traite illumination ", i_ill
#endif
    do iz = 1, nz
       call dfftw_plan_dft_2d(plan2f,  nxfft, nyfft, ztemp, ztemp2, &
            FFTW_FORWARD ,FFTW_ESTIMATE)
       call dfftw_plan_dft_2d(plan2b,  nxfft, nyfft, ztemp, ztemp2, &
            FFTW_FORWARD ,FFTW_ESTIMATE)
    end do

    params_direct%data( :, :, i_ill) = 0.0D0
    do iz = 1, nz
#ifdef DEBUG
       print*, __FILE__, __LINE__, " traite plan z ", iz
#endif
       ! fft de  \rho * I
       ztemp((nxfft-nx)/2:(nxfft+nx)/2-1, (nxfft-nx)/2:(nxfft+nx)/2-1) = &
               params_direct%rho( :, :, iz ) * params_direct%ill( :, :, iz, i_ill)
       call dfftw_execute_dft ( plan2f , ztemp, ztemp2 )
       ztemp = ztemp2 * fft_psf(:, :, iz)  ! produits des fft
       call dfftw_execute_dft(plan2b, ztemp, ztemp2)
       ztemp = ztemp2/dble(nxfft*nyfft)
       !ztemp = ztemp2 / (params_direct%lx/dble( params_direct%Nx ) * params_direct%ly/dble( params_direct%Ny ))
       call fftshift(ztemp, h2) !  on periodise a nouveau pour remettre dans l'ordre
       h( 1:nx, 1:ny ) = h2((nxfft-nx)/2:(nxfft+nx)/2-1, (nxfft-nx)/2:(nxfft+nx)/2-1)

       if (isnan(maxval(real(h))) .or. isnan(maxval(aimag(h)))) stop '"h" is a NaN'

       !call save_conv_rhoIpsf(iz, h)
       ! il faut multiplier le resultat par deltax deltay !!!
       params_direct%data( :, :, i_ill ) = h(:, :) &
               * (params_direct%deltax) * (params_direct%deltay)
    enddo
    !
#ifdef DEBUG
    call save_data() ! sauve params_direct%data(:, :, :)
#endif

    deallocate(ztemp)
    deallocate(ztemp2)
      deallocate( h )
      deallocate( h2 )
  end subroutine direct

  subroutine save_conv_rhoIpsf(iz, h)
    integer, intent(in) :: iz
    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: h

    integer :: ix, iy, nx, ny
    CHARACTER(len=255) :: cwd
    character(len=4)::tag

    nx      = params_direct%Nx
    ny      = params_direct%Ny

#ifdef DEBUG
    write(17,*) "# verif convolution de rho I et psf pour z ", iz, &
         & " voir fort.17 max:", maxval(real(h)), maxval(aimag(h))
    do ix=1,nx
       do iy=1,ny
          write(17,*) ix, iy, real(h(ix,iy)), aimag(h(ix,iy))
       enddo
    enddo
    close(17)
    CALL getcwd(cwd)
    print*, __FILE__, __LINE__, " verif convolution de rho I et psf pour z ", iz, &
         & " voir fort.17 max du resultat:", maxval(real(h)), maxval(aimag(h))
    print*, __FILE__, __LINE__, ' set dgrid3d 128,128; set hidden3d; &
         & splot "' // TRIM(cwd) // '/fort.17" using 1:2:3 with lines'
#endif

  end subroutine save_conv_rhoIpsf

  ! on stocke le resultat dans un fichier texte, il s'agit de donnees simulees
  ! pour les donnees experimentales, elles sont sauvees au format tiff, sous forme
  ! d'images 2d (x,y), chaque image correspond a une realisation de speckle
  subroutine save_data()
    implicit none

    CHARACTER(len=255) :: cwd
    character(len = LENGTH) :: fichier
    integer :: ix, iy, nx, ny
    integer :: i_ill
    character(len=4)::tag

    nx      = params_direct%Nx
    ny      = params_direct%Ny

    print *, __FILE__, __LINE__, "TODO une seule illumination!"
    i_ill = 1

    print *, "sauvegarde du resultat"
    !  stockage résultat
    !
    CALL getcwd(cwd)
    write(tag,'(I4.4)') i_ill
    fichier = TRIM(cwd) // '/data_' // trim(tag) // '.txt'
    print*, __FILE__, __LINE__, " ecriture ",  trim(fichier)
    open(unit=11, file=trim(fichier))
    ! TODO on ne trace que pour une illumination, il faut les faire toutes !?
    do ix=1,nx
       do iy=1,ny
          write(11,*) ix, iy, params_direct%data(ix,iy, i_ill)
       enddo
    enddo
    close(11)
    print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
    print *, __FILE__, __LINE__, 'splot "'// trim(fichier) &
         & // '" using 1:2:3 with lines'
  end subroutine save_data

end module mod_fluo_direct
