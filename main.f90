!  Ce code calcule l'image de fluorescence a partir de la densite de
!  fluorophores, l'intensite dans le domaine des fluorophores et la
!  psf du microscope. La convolution est calcule a l'aide des FFT pour
!  les fonctions reelles. Les FFT sont dans ce cas hermitiennes.
!
! Kamal BELKEBIR (novembre 2019)
!
program conv_fluo

  use mod_cte_math
  use mod_fft_param

  use mod_conv_fluo

  implicit none

  integer :: nx, ny, ix, iy

  !  f et g sont les fonctions reelles (input); et h est aussi reele
  !  (output). h = f conv g.
  !
  double precision :: f(nxm,nym), g(nxm, nym)
  double complex :: h(nxfft, nyfft)
  double complex, dimension(nxfft,nyfft)  :: conv_analy
  double precision, dimension(nxfft) :: kx
  double precision, dimension(nyfft) :: ky


  !  des parametres juste pour l'exemple
  !
  double precision :: x(nxm), y(nym), r, lx, ly
  double precision :: x_double(nxfft), y_double(nyfft)
  double precision :: a
  double precision :: deltax, deltay
  double precision :: sigma2

  nx=64
  ny=64

  ! echantillonage dans l'espace direct
  !
  lx=l
  ly=l
  deltax=lx/dble(nx)
  deltay=ly/dble(ny)

  ! RMQ : le centre est en (nx/2+1, ny/2+1)
  !
  print*,'calcul des points echantillonage dans l espace direct'
  do ix = 1,nx
     x(ix)=-lx/2.0d0+dble(ix-1)*deltax
  enddo
  do iy = 1,ny
     y(iy)=-ly/2.0d0+dble(iy-1)*deltay
  enddo


  !  deux gaussiennes identiques centrees de largeur sigma Ceci est
  !  juste un exemple. f representera rho*I et g la psf.
  !
  !  but: on souhaite calculer (f=rho*I) convoluee avec g=psf
  !

  ! definition de deux gaussiennes 2D d'ecart type sigma dont on veut
  ! calculer la convolution
  !
  a=1.0d0/(2.0d0*sigma*sigma)
  do ix=1,nx
     do iy=1,ny
        r= (x(ix)-x0)**2+(y(iy)-y0)**2
        r= r*a
        f(ix,iy) = exp(-r)*(1.0d0/(sigma*sigma*twopi))   
        g(ix,iy) = exp(-r)*(1.0d0/(sigma*sigma*twopi))   
     enddo
  enddo

  !  pour la convolution on calcule les tf des deux fonctions, on les
  !  multiplie dans FOURIER puis en revient dans l'espace
  !  direct. Pour le calcul des tf on va suivre la meme demarche
  !  qu'en 1D. Le reste est trivial !

  !  on sauvegarde les signaux originaux de l'espace direct. Deja
  !  calcules plus haut
  !
  open(unit =11, file = 'gauss_f_2d.mat')
  open(unit =12, file = 'gauss_g_2d.mat')
  do iy=1,ny
     do ix=1,nx
        write(11,*) f(ix,iy)
        write(12,*) g(ix,iy)
     enddo
  enddo
  close(11)
  close(12)


  h = conv_2D(f, g, nx, ny, kx, ky)


  sigma2=2.0*sigma
  a=1.0d0/(2.0d0*sigma2*sigma2)
  do ix=1,nxfft
     do iy=1,nyfft
        r= (x_double(ix)-x0)**2+(y_double(iy)-y0)**2
        r= r*a
        conv_analy(ix,iy) = exp(-r)*(1.0d0/(sigma2*sigma2*twopi))   
     enddo
  enddo
  open(unit=11, file ='real_conv2d_analy.mat')
  open(unit=12, file ='imag_conv2d_analy.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) x_double(ix), y_double(iy), real(conv_analy(ix,iy))
        write(12,*) x_double(ix), y_double(iy), aimag(conv_analy(ix,iy))
     enddo
  enddo
  close(11)
  close(12)



  !  FIN
  ! ------
end program conv_fluo
