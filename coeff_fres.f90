program coeff_fres
  implicit none
  
  double precision :: theta, k0, lambda, twopi, lambda1, lambda2, profp1, profp2
  double complex   :: zn1, zn2, zeps1, zeps2
  double complex   :: zk1, zk2, beta1, beta2, zalpha1
  double complex   :: rpara, tpara, rortho, tortho
  
  ! constantes maths
  double precision, parameter :: pi = acos(-1.0d0)
  double complex, parameter :: ucomp = cmplx(1.0d0,0.0d0), icomp=cmplx(0.0d0,1.0d0)

  ! constantes physiques
  double precision c0, eps0, mu0
  parameter ( c0 = 299792458.0D0)
  parameter ( mu0   = 4.0D-7*pi)
  parameter ( eps0  = 1.0D0/(c0*c0*mu0))

  
  twopi  = 2.0d0*pi
  lambda = twopi             ! longueur d'onde rouge
  zn1    = cmplx(1.5D0,0.0)  ! indice du milieu 1 (ici de l'air)
  zn2    = cmplx(1.0D0,0.0)  ! indice du milieu 2 (ici du verre)
  !
  ! relation indice-permittivité : n=\sqrt(\epsilon_r)
  !
  zeps1  = zn1*zn1
  zeps2  = zn2*zn2
  
  theta  = 42.0d0            ! angle d'incidence en degrés
  theta  = (theta*pi)/180.0d0



  !
  k0     = twopi/lambda ! nombre d'onde dans le vide
  zk1    = k0*zn1       ! nombre d'onde dans le milieu 1
  zk2    = k0*zn2       ! nombre d'onde dans le milieu 2
  !
  lambda1 = twopi/real(zk1) ! longueur d'onde dans le milieu 1
  lambda2 = twopi/real(zk2) ! longueur d'onde dans le milieu 2
  !
  profp1 = epaisseur_peau(zk1)
  profp2 = epaisseur_peau(zk2)

  zalpha1 = zk1*sin(theta)

  ! CAS : E_//
  beta1 = sqrt(zk1*zk1-zalpha1*zalpha1)
  beta2 = sqrt(zk2*zk2-zalpha1*zalpha1)

  print*,'beta2 : ', beta2
  rpara = (beta1-beta2)/(beta1+beta2)
  tpara = (2.0d0*beta1)/(beta1+beta2)

  ! CAS : H_//
  beta1 = sqrt(zk1*zk1-zalpha1*zalpha1)/zeps1
  beta2 = sqrt(zk2*zk2-zalpha1*zalpha1)/zeps2

  rortho = (beta1-beta2)/(beta1+beta2)
  tortho = (2.0d0*beta1)/(beta1+beta2)

  print*,'rpara  : ', rpara
  print*,'tpara  : ', tpara

  print*,'rortho  : ', rortho
  print*,'tortho : ', tortho

contains
  function epaisseur_peau(zk)
    double precision epaisseur_peau
    double complex zk

    if (aimag(zk) /= 0.0d0) then
       epaisseur_peau = 1.0d0/aimag(zk)
    else
       print*,'Milieu sans pertes'
    endif
  end function epaisseur_peau

end program coeff_fres
