
#### Récupération du projet
```
git clone git@gitlab.fresnel.fr:henry/fluo.git
cd fluo
```

#### compilation
```
bash make_bins
```

attention, au mesocentre, il faut recompiler fftw parce qu'il n'arrive pas à trouver celui qu'on charge avec la commande module
```
module purge; module load userspace gcc/11.2.0

```
pour FFTW3, s'inspirer de la page https://github.com/xtensor-stack/xtensor-fftw/issues/52
```
FFTW_VERSION=3.3.9
curl -L http://www.fftw.org/fftw-${FFTW_VERSION}.tar.gz | tar xz
mkdir build && cd $_
CC=/trinity/shared/apps/custom/x86_64/gcc-11.2.0/bin/gcc \
cmake  -DCMAKE_INSTALL_PREFIX=~/LIBS -DBUILD_SHARED_LIBS=OFF -DENABLE_OPENMP=ON -DENABLE_THREADS=ON -DENABLE_LONG_DOUBLE=ON \
../fftw-${FFTW_VERSION}
make -j $(nproc) install
rm -r *
CC=/trinity/shared/apps/custom/x86_64/gcc-11.2.0/bin/gcc cmake -DCMAKE_INSTALL_PREFIX=~/LIBS -DBUILD_SHARED_LIBS=OFF -DENABLE_OPENMP=ON -DENABLE_THREADS=ON -DENABLE_FLOAT=ON -DENABLE_SSE=ON -DENABLE_SSE2=ON -DENABLE_AVX=ON -DENABLE_AVX2=ON ../fftw-${FFTW_VERSION}
make -j $(nproc) install
rm -r *
CC=/trinity/shared/apps/custom/x86_64/gcc-11.2.0/bin/gcc cmake -DCMAKE_INSTALL_PREFIX=~/LIBS -DBUILD_SHARED_LIBS=OFF -DENABLE_OPENMP=ON -DENABLE_THREADS=ON -DENABLE_SSE=ON -DENABLE_SSE2=ON -DENABLE_AVX=ON -DENABLE_AVX2=ON ../fftw-${FFTW_VERSION}
make -j $(nproc) install
```
une fois fait, on peut compiler fluo:
``` 
cmake -DFFTW_ROOT=~/LIBS ..
```
puisque FFTW est installée là

encore un problème au mesocentre, il trouve bien la lib FFTW, mais pas tout, car à l'édition de liens, il oublie le chemin:
```
/testfft.exe.dir/fichiers.cpp.o -o testfft.exe  -lboost_system-mt -lboost_thread-mt -lpthread -lboost_regex-mt -lboost_program_options-mt
-lboost_chrono-mt -lboost_date_time-mt -lboost_atomic-mt -lfftw3l_omp -lfftw3l -lgomp -lpthread
/usr/bin/ld: cannot find -lfftw3l_omp
/usr/bin/ld: cannot find -lfftw3l
collect2: error: ld returned 1 exit status
```
et on le résoud temporairement en l'ajouant à la main
```
[ghenry@login01 build] > /trinity/shared/apps/custom/x86_64/gcc-11.2.0/bin/c++ -g -gdwarf-2 -O0 -std=c++11 -Wall -Wextra -Wconversion -Wshadow -Wpedantic -Wl,-Map=yoyoIto.map -DDEBUG -DHAVE_FFTW3 -fopenmp -DNOT_PARAMETRIC -rdynamic CMakeFiles/testfft.exe.dir/test_fft.cpp.o CMakeFiles/testfft.exe.dir/fct_fft.cpp.o CMakeFiles/testfft.exe.dir/fichiers.cpp.o -o testfft.exe  -lboost_system-mt -lboost_thread-mt -lpthread -lboost_regex-mt -lboost_program_options-mt -lboost_chrono-mt -lboost_date_time-mt -lboost_atomic-mt -L/home/ghenry/LIBS/lib64/ -lfftw3l_omp -lfftw3l -lgomp -lpthread
```

#### Exécution:

voir le script lance_simus.sh comme exemple:
```{shell, attr.source='.numberLines'}
#!/bin/bash
# on utilise le code direct pour créer un fichier de mesures
chemin="/home/me/projets/fluo/runs/tests/"
for mytest in "portes" "gaussiennes" "deuxgaussiennesidentiques"
do
    echo $mytest
    cd ${chemin}/${mytest}
    METH="direct"
    MYBIN=fluo_$METH
    ../../../cmake-build-release/$MYBIN fluo_$METH.IN
    if [ $? != 0 ]; then
	echo "Erreur de " $MYBIN
	exit 1
    fi
    METH="inverse"
    MYBIN=fluo_$METH
    ../../../cmake-build-release/$MYBIN fluo_$METH.IN
    if [ $? != 0 ]; then
	echo "Erreur de " $MYBIN
	exit 1
    fi
done
```

le fichier en argument contient les paramètres de la simulation, et se présente sous cette forme:
```{bash, attr.source='.numberLines'}
&geometrieXYZ ! concerne aussi la psf? voir avec Kevin
lx=12 ly=12 lz=1  !! dimension du domaine en nm
Nx=128 Ny=128 	!! decoupage du plan xy
Nz=1           !! nombre de plans en Z
/
&illuminations 
n_illum=1 
/
&fichiers ! mettre soit un des mots cles 'One', 'Porte', 'Gaussienne', 'Bessel'
! ou un nom de fichier. Ce qui n'est pas reconnu comme mot-cle est pris comme nom de fichier
!la densite etant ce qu'on cherche, peu importe ce qu'on met
densite="One"
illumination="One"
psf="Gaussienne"
measures="./data_0001.txt"
write_to_files=.true.
/
&iterations
nb_iter=100
tol=1e-3
/
&capteur  ! le capteur utilise par Kevin
Nxc=1392
Nyc=1040
/
&fft  ! paramètres de la FFT
Nxfft=1024
Nyfft=1024
/

```
qui est la forme habituelle des [NAMELIST en Fortran] (https://cyber.dabamos.de/programming/modernfortran/namelists.html).

Comme c'est une inversion qu'on cherche à faire, le paramètre **densite** n'est pas interprété, et le fichier de mesures doit être valide.
Le code s'arrête au bout de **nb_iter** itérations.
Les dimensions du domaine ne sont pas en nanomètre **TODO**