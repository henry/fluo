! Created by me on 20/09/2021.

module fftw3
  ! provides Fortran type names constants which corresponds to C type
  use, intrinsic :: iso_c_binding
  ! provides constants for FFTW3 usage - include file located in /usr/include
  include 'fftw3.f03'
end module fftw3
