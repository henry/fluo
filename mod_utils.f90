!================================================================
! Created by me on 26/05/2021.
! Henry GERARD
! Kamal BELKEBIR (juin 2021)
!================================================================
module mod_utils
  use mod_maths
  use mod_params

  implicit none

  !double precision, parameter :: sigma=1.0d0
  double precision, parameter :: delta = 3.0d0 ! largeur de la porte centree
  double precision, parameter :: x0=0.0d0 ! pourquoi en parametres ?
  double precision, parameter :: y0=0.0d0 !

contains

  ! porte 2D de largeur e, symetrique dans les deux dimensions
  function porte2d(x, y, e)
    double precision porte2d
    double precision, intent(in) :: x, y, e

    if ((abs(x) <= 0.5D0 * e) .and. (abs(y) <= 0.5D0 * e))then
       porte2d = 1.0D0
    else
       porte2d = 0.0D0
    endif

  end function porte2d


  !================================================================
  ! on remplit un tableau tab avec le contenu d'un fichier ou avec
  ! des fonctions mathematiques comme une gaussienne
  ! entrées:
    !  type: comment remplir le tableau tab
    ! xgrid, ygrid: coordonnées des points de discrétisation
    ! nom: contexte du tableau (rho ou psf)
  ! sorties:
    ! tab: tableau 3D
  ! TODO pas de différence en z ?
  subroutine set_signal( type, tab, x_grid, y_grid, nom )
    integer, intent(in) :: type ! type d'algo pour le signal
    double precision, dimension(:,:, :), allocatable, intent(inout)   :: tab
    double precision, dimension(:), allocatable, intent(in) :: x_grid, y_grid
    CHARACTER(len=*) :: nom

    integer          :: ix, iy, iz
    CHARACTER(len=255) :: cwd
    double precision :: sigma

    if (type == FILE) then
       call lecture(nom, tab)
    elseif (type == GAUSS) then
       sigma = 1.0d0
       call gaussienne2D(x_grid, y_grid, tab, 0.0d0, 0.0d0, sigma)
    elseif (type == PORTE) then
#ifdef DEBUG
       print *, __FILE__, __LINE__, 'porte 2d symetrique de largeur ', delta
#endif
       ! une fonction porte 2D de largeur e
       do ix=1, params_direct%Nx
          do iy=1, params_direct%Ny
             do iz=1, params_direct%Nz
                tab( ix, iy, iz ) = porte2d(x_grid(ix), y_grid(iy), delta)
             enddo
          enddo
       enddo
    else
       ! fonction uniforme
       tab( :, :, : ) = 1.0D0
    endif

    !  on sauvegarde les signaux originaux de l'espace direct.
    !
    iz = 1
  call save_signal(tab, nom, iz)

  end subroutine set_signal

  !================================================================
  !initialise l'illumination', retourne le tableau ill, avec une forme/algo définie dans le fichier params
  subroutine set_illum( x_grid, y_grid )
    double precision, dimension(:), allocatable, intent(inout) :: x_grid, y_grid

#ifdef DEBUG
    print*, __FILE__, __LINE__, "on fixe l'illumination à 1"
#endif
    params_direct%ill( :,:,:,: ) = 1.0D0

    ! TODO prevoir autres types d'illumination
  end subroutine set_illum


  !================================================================
  ! Maillage Cartesien : retourne les coordonnees d'un plan (x,y)
  subroutine maille_xy( x, y )

    double precision, dimension(:), allocatable, intent(inout) :: x, y

    integer          :: nx, ny, ix, iy
    double precision :: lx, ly
    double precision :: deltax, deltay

    nx     = params_direct%Nx
    ny     = params_direct%Ny
    ! echantillonage dans l'espace direct
    !
    lx     = params_direct%lx
    ly     = params_direct%ly

    deltax = lx/dble( nx )
    deltay = ly/dble( ny )

    ! RMQ : le centre est en (nx/2+1, ny/2+1)
    !
    print*, __FILE__, __LINE__, 'calcul des points echantillonnage dans l espace direct'
    do ix = 1,nx
       x( ix ) = -lx/2.0d0 + dble( ix-1 )*deltax
    enddo
    !
    do iy = 1,ny
       y( iy ) = -ly/2.0d0 + dble( iy-1 )*deltay
    enddo
  end subroutine maille_xy
  !================================================================

  !================================================================
  ! Maillage double : pour les fonctions périodisées
  subroutine maille_double_xy( x_double, y_double )

    double precision, dimension(:), allocatable, intent(inout) :: x_double, y_double

    integer          :: ix, iy, nxfft, nyfft
    double precision :: l2x, l2y
    double precision :: deltax2, deltay2

    nxfft = 2*params_direct%nx
    nyfft = 2*params_direct%ny
    l2x = 2.0*params_direct%lx
    deltax2=2.0*l2x/dble(nxfft)
    allocate(x_double(nxfft))
    do ix=1,nxfft
       x_double(ix)=-params_direct%lx+dble(ix-1)*deltax2
    enddo

    l2y = 2.0*params_direct%ly
    deltay2=2.0*l2y/dble(nyfft)
    allocate(y_double(nyfft))
    do iy=1,nyfft
       y_double(iy)=-params_direct%ly+dble(iy-1)*deltay2
    enddo
  end subroutine maille_double_xy
  !================================================================

  !================================================================
  ! Les frequences spatiales
  subroutine init_kxky( lx, ly, kx, ky ) ! init_kxky pas bon comme
    ! nom. Ce n'est pas une
    ! initialisation. ce sont les
    ! frequences spatiales du
    ! probleme.
    double precision :: lx, ly
    double precision, dimension(:), allocatable, intent(out) :: kx, ky

    integer :: nx, ny, ix, iy, i, j
    double precision :: deltax, deltay
    double precision :: deltakx, deltaky
    integer  :: nxfft, nyfft

    nx = size(kx)
    ny = size(ky)
    ! echantillonage dans l'espace direct
    !
    deltax = lx/dble( nx )
    deltay = ly/dble( ny )

    !  echantillonage dans l'espace de FOURIER RMQ : la frequence
    !  spatiale nulle est en nfft/2+1. Les librairies permettant de
    !  calculer les FFT considèrent des supports positifs. Faut tenir
    !  compte de cela.
    !
    nxfft = 2*nx
    nyfft = 2*ny

    ! pourquoi ce test ? Je pense qu'il faut fixer nxfft et nyfft a
    ! une valeur et traiter nx, ny comme devant etre inferieurs a
    ! cette valeur
    if ( ( nxfft > 2*nx ) .or. ( nyfft > 2*ny ) ) then
       WRITE( *,* ) 'ERROR, nxfft or nyfft too big, STOPPING'
       stop 1
    end if

    deltakx  = twopi/( deltax*dble( nxfft ) )
    deltaky  = twopi/( deltay*dble( nyfft ) )
    allocate(kx(nxfft))
    do ix = -nxfft/2,nxfft/2-1
       i       = ix+nxfft/2+1
       kx( i ) = dble( ix )*deltakx
    enddo
    !
    allocate(ky(nxfft))
    do iy = -nyfft/2, nyfft/2-1
       j       = iy+nyfft/2+1
       ky( j ) = dble( iy )*deltaky
    enddo

  end subroutine init_kxky

  ! gaussienne 2D, en z c'est la même chose qui est répété
  ! entrées:
  ! le maillage dans les tableaux x et y
  ! le centre en x0,y0
  ! la largeur de la gaussienne: sigma (paramètre global)
  ! sortie:
  !  le tableau f
  subroutine gaussienne2D( x, y, f, x0, y0, sigma)
    double precision, dimension(:), allocatable :: x, y
    double precision, dimension(:,:,:), allocatable :: f
    double precision :: x0, y0  ! indice du plan en z
    double precision, intent(in) :: sigma

    double precision :: a, r, amplitude
    integer :: ix, iy, iz

    ! definition de  gaussienne 2D d'ecart type sigma
    ! https://fr.wikipedia.org/wiki/Fonction_gaussienne#Fonction_gaussienne_en_deux_dimensions
    a=1.0d0/(2.0d0*sigma*sigma)
    amplitude = 1.0d0/(sigma*sigma*twopi)
!#ifdef DEBUG
    print*, __FILE__, __LINE__, "gaussienne d'amplitude ", amplitude
    print*, __FILE__, __LINE__, " d'ecart en x ", sigma, &
         " pour un domaine de ", params_direct%Lx
    print*, __FILE__, __LINE__, " d'ecart en y ", sigma, &
         " pour un domaine de ", params_direct%Ly
    if ((x0 == 0.0d0) .and. (y0 == 0.0d0)) then
       print*, __FILE__, __LINE__, " centree a l'origine "
    else
       print*, __FILE__, __LINE__, " decentree de ", x0, y0
    endif
!#endif
    do ix=1,params_direct%Nx
       do iy=1,params_direct%Ny
          do iz=1,params_direct%Nz
             r= (x(ix)-x0)**2+(y(iy)-y0)**2
             r= r*a
             f(ix, iy, iz) = amplitude * exp(-r)
          end do
       enddo
    enddo
    !       call gaussienne2D(x_grid, y_grid, tab, sigma, 0.0d0, 0.0d0)
  end subroutine gaussienne2D

  ! somme de deux gaussienne 2D pour voir la resolution (pur exercice)
  ! entrées:
  ! le maillage dans les tableaux x et y
  ! le centre en x1,y1 de la première gaussienne
  ! la largeur sigma1 de première gaussienne
  ! sortie:
  !  le tableau f
  subroutine somme2gaussienne2D( x, y, f, x1, y1, sigma1, x2 ,y2, sigma2)
    double precision, dimension(:), allocatable :: x, y
    double precision, dimension(:,:,:), allocatable :: f
    double precision :: x1, y1, x2, y2  ! indice du plan en z
    double precision, intent(in) :: sigma1, sigma2

    double precision :: a1, a2, r, amplitude1, amplitude2
    integer :: ix, iy, iz

    ! definition de  gaussienne 2D d'ecart type sigma
    ! https://fr.wikipedia.org/wiki/Fonction_gaussienne#Fonction_gaussienne_en_deux_dimensions
    a1=1.0d0/(2.0d0*sigma1*sigma1)
    amplitude1 = 1.0d0/(sigma1*sigma1*twopi)
    a2=1.0d0/(2.0d0*sigma2*sigma2)
    amplitude2 = 1.0d0/(sigma2*sigma2*twopi)
    do ix=1,params_direct%Nx
       do iy=1,params_direct%Ny
          do iz=1,params_direct%Nz
             r= (x(ix)-x1)**2+(y(iy)-y1)**2
             r= r*a1
             f(ix, iy, iz) = amplitude1 * exp(-r)
             r= (x(ix)-x2)**2+(y(iy)-y2)**2
             r= r*a2
             f(ix, iy, iz) = f(ix, iy, iz) + (amplitude2 * exp(-r))
          end do
       enddo
    enddo
    !       call gaussienne2D(x_grid, y_grid, tab, sigma, 0.0d0, 0.0d0)
  end subroutine somme2gaussienne2D




  ! on lit les données dans un fichier texte
  ! \rho (densité) valeur sur une seule colonne, pas de metadonnées
  subroutine init_fg_with_files( x, y, f, g)
    double precision, dimension(:), allocatable :: x, y
    double precision, dimension(:,:), allocatable :: f, g

    integer :: nx, ny, ix, iy
    logical :: exists
    integer :: io, i
    doubleprecision :: val

    nx = params_direct%Nx
    ny = params_direct%Ny

    nx = 5
    ny = 5
    ! lecture du fichier contenant \rho
    !
    inquire(file=params_direct%file_rho, exist=exists)
    if (.not. exists) then
       WRITE(*,*) 'Check input.  Something was wrong ', params_direct%file_rho
       STOP 2
    end if
    i = 0
    ix = 1
    iy = 1
    open(10, file=params_direct%file_rho)
    do
       read(10, *, iostat=io) val
       IF (io > 0) THEN
          WRITE(*,*) 'Check input.  Something was wrong'
          STOP 3
       ELSE IF (io < 0) THEN
#ifdef DEBUG
          print*, __FILE__, __LINE__, 'fin lecture du fichier ', params_direct%file_rho
          print*, __FILE__, __LINE__, " de taille ", i
#endif
          STOP 4
       ELSE
          i = i + 1
          ix = modulo(i, nx+1)
          if (ix == nx) then
             iy = iy + 1
          end if
#ifdef DEBUG
          print*, __FILE__, __LINE__, i, ix, iy
#endif
          !f(ix,iy) = val
       END IF

    end do

    stop 5

  end subroutine init_fg_with_files

  subroutine savecmplx2txt( h2, fichier)
    double complex, dimension(:,:)   :: h2
    character(len=*), intent(in):: fichier

    integer :: ix, iy
    integer  :: nx, ny, nxfft, nyfft
    doubleprecision :: lx2, ly2, deltax2, deltay2
    double precision, dimension(:), allocatable :: x_double, y_double

#ifdef DEBUG
    print*, __FILE__, __LINE__,'ecriture du fichier ', 'real' // fichier
    print*, __FILE__, __LINE__,'ecriture du fichier ', 'imag' // fichier
#endif
    open(unit=11, file ='real' // fichier)
    open(unit=12, file ='imag' // fichier)

    nx = params_direct%Nx
    ny = params_direct%Ny

    nxfft = params_fft%Nxfft
    nyfft = params_fft%Nyfft

    allocate(x_double(nxfft))
    allocate(y_double(nyfft))
    lx2=2.0*params_direct%lx
    deltax2=2.0*lx2/dble(nxfft)
    do ix=1,nxfft
       x_double(ix)=-params_direct%lx+dble(ix-1)*deltax2
    enddo
    ly2=2.0*params_direct%ly
    deltay2=2.0*ly2/dble(nyfft)
    do iy=1,nyfft
       y_double(iy)=-params_direct%ly+dble(iy-1)*deltay2
    enddo

    do iy=1,nyfft
       do ix=1,nxfft
          write(11,*) x_double(ix), y_double(iy), real(h2(ix,iy))
          write(12,*) x_double(ix), y_double(iy), aimag(h2(ix,iy))
       enddo
    enddo
    close(11)
    close(12)

    deallocate(x_double)
    deallocate(y_double)
  end subroutine savecmplx2txt

  subroutine savereal2txt( x_grid, y_grid, h, fichier )
    double precision, dimension(:), allocatable :: x_grid, y_grid
    double precision, dimension(:,:), allocatable   :: h
    character(len=*), intent(in):: fichier

    integer :: ix, iy
    integer  :: nx, ny

#ifdef DEBUG
    print*, __FILE__, __LINE__,'ecriture du fichier ', 'real' // fichier
#endif

    open(unit=11, file ='real' // fichier)

    nx = params_direct%Nx
    ny = params_direct%Ny

    ! TODO utiliser params%wr pour ecrire ou pas? dans du hdf5?
    do iy=1,ny
       do ix=1,nx
          write(11,*) x_grid(ix), y_grid(iy), h(ix,iy)
       enddo
    enddo
    close(11)

  end subroutine savereal2txt

  function maxtab(tab, imax, jmax)
    ! retourne la premiere valeur max rencontree
    double precision, dimension(:,:) :: tab
    double precision :: maxtab
    integer, intent(out) :: imax, jmax

    integer :: i, j

    ! tout le tableau est parcouru, pas très optimal
    maxtab = 0.0d0
    imax = 1
    jmax = 1
    do i = 1, size(tab, 1)
       do j = 1, size(tab, 2)
          if (tab(i,j) > maxtab) then
             maxtab = tab(i,j)
             imax = i
             jmax = j
          end if
       end do
    end do
  end function maxtab

end module mod_utils

