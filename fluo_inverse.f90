!  Ce code de problème inverse calcule la permitivité \rho
! à partir de la minisation d'une fonction de coût
! voir https://gitlab.fresnel.fr/henry/thesis/-/blob/master/invprobl.tex

! Principe:
! à partir de mesures expérimentales et de l'illumination qui a permis
! de les produire, on calcule un résidu en se donnant une estimation
! initiale de \rho. S'ensuivent un calcul de gradient, de coefficient alpha
! et un \rho_{n}

! le code est testé avec des cas tests simples (porte, gaussienne, etc...)

! Kamal BELKEBIR (novembre 2019)
! Gérard HENRY (mai 2021)
!
program fluo_inverse

  use FFTW3
  use mod_maths
  use mod_params  !! pour lire dans un fichier les paramètres de la simu
  use mod_utils    !! contient les routines pour la discretisation dans l'espace direct et de Fourier
  use iso_fortran_env
  use, intrinsic :: iso_c_binding
  use mod_conv2d
  use mod_fft, only : fftshift
  use mod_fluo_direct
  use mod_aide
  use mod_GC_fluo

  implicit none

  !=======================================================================
  ! domaine d'investigation Omega restreint a un plan
  integer :: nx, ny, nz
  !=======================================================================
  double complex, dimension(:,:,:), allocatable   :: fft_psf
  !=======================================================================
  ! variables locales
  integer          :: i, ix, iy, iz, i_ill, iter
  CHARACTER(len=32) :: arg

  double precision, dimension(:), allocatable :: x_grid, y_grid
  complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: ztemp, ztemp2

  integer :: nxfft, nyfft
  integer(8)       :: plan2f, plan2b
  integer          :: iret, omp_get_max_threads ! pour FFT en


  ! le programme doit être lancé avec un argument: ./monprog mon-fichier-de-parametres
  ! si l' argument n existe pas on sort
  IF( COMMAND_ARGUMENT_COUNT() /= 1 )THEN
     WRITE(*, *)'ERROR, ONE COMMAND-LINE ARGUMENTS REQUIRED, STOPPING'
     WRITE(*, *)'Usage: ./fluo_inverse  ../fluo_inverse.IN'
     STOP 1
  ENDIF
  DO
     CALL get_command_argument(i, arg)
     IF (LEN_TRIM(arg) == 0) EXIT
     if ((TRIM(arg) == "--help") .or. (TRIM(arg) == "-h")) then
        call affiche_aide()
        STOP 1
     end if
     i = i+1
  END DO

#ifdef DEBUG
  print*, __FILE__, __LINE__, 'This file was compiled by ', &
       compiler_version(), ' using the the options ', &
       compiler_options()
#endif

  call lecture_params()
#ifdef DEBUG
  call affiche_params()
#endif

  nx      = params_direct%Nx
  ny      = params_direct%Ny
  nz      = params_direct%Nz

  allocate( x_grid( nx ) )
  allocate( y_grid( ny ) )

  ! calcul les points de discretisation dans l'espace direct
  call maille_xy( x_grid, y_grid )

  allocate( params_direct%rho( nx, ny, nz ) )
  ! TODO on ne connait pas l'illumination en général, K.A. parle de bouger simplement
  ! le diffuseur entre deux images du même objet
  allocate( params_direct%ill( nx, ny, nz, params_direct%nb_illum ) )
  allocate( params_direct%psf( nx, ny, nz ) )

  nxfft = params_fft%Nxfft
  nyfft = params_fft%Nyfft
  allocate( fft_psf( nxfft, nyfft, nz )  )
  allocate( ztemp(  nxfft, nyfft )  )
    ztemp = 0.0d0
  allocate( ztemp2(  nxfft, nyfft )  )
    ztemp2 = 0.0d0

  ! pour stocker les images ou les calculs intermediaires
  allocate( params_direct%data( nx, ny, params_direct%nb_illum ) )

  ! init de la lib FFTW3
  call dfftw_init_threads(iret)
  call dfftw_plan_with_nthreads(omp_get_max_threads())

  ! TODO les plans devraient aussi dépendre de z
  !do iz = 1, nz
  call dfftw_plan_dft_2d(plan2f,  nxfft, nyfft, ztemp, ztemp2, &
       FFTW_FORWARD ,FFTW_ESTIMATE)
  call dfftw_plan_dft_2d(plan2b,  nxfft, nyfft, ztemp, ztemp2, &
       FFTW_FORWARD ,FFTW_ESTIMATE)
  !end do
!!! on fixe la PSF: params_direct%psf , avec une forme/algo définie dans le fichier params
  ! normalement, la PSF est connue grace aux mesures experimentales
  ! donc on devrait la lire dans un fichier
  ! TODO pour l'instant  on rentre une PSF, avec une forme/algo définie dans le fichier params
  call set_signal( params_direct%algopsf, params_direct%psf, x_grid, y_grid, "psf2d" )

  ! on calcule les FFT de la PSF pour chaque plan z
  do iz = 1, nz
      ztemp((nxfft-nx)/2:(nxfft+nx)/2-1, (nxfft-nx)/2:(nxfft+nx)/2-1) = params_direct%psf(1:nx, 1:ny, iz)
     call dfftw_execute_dft ( plan2f, ztemp, ztemp2 )
     fft_psf(:, :, iz) = ztemp2(:, :)
  enddo

  write (*, *) " ***********************"
  write (*, *) " on commence l'inversion"
  write (*, *) " ***********************"
  call inverse(fft_psf)


  deallocate( x_grid )
  deallocate( y_grid)
  deallocate( params_direct%psf )
  deallocate( fft_psf )
  !  FIN
  ! ------


contains

  subroutine inverse(fft_psf)
    use mod_maths
    implicit none

    double complex, dimension(:,:,:), allocatable, intent(inout)   :: fft_psf

    double precision, dimension(:,:,:), allocatable :: rhon1
    double precision :: alpha
    double precision :: coeff0, coeff1, coeff2

    ! parametre 3D
    double precision, dimension(:,:,:), allocatable :: grad, gradn1
    double precision, dimension(:,:,:), allocatable :: dn, dn1
    double precision, dimension(:,:), allocatable :: temp
    !complex ( C_DOUBLE_COMPLEX ), dimension(:,:, :), allocatable :: residu
    double precision :: fcout, W_gamma
    double precision, dimension(:,:,:), allocatable :: residu, mes ! x, y, nb_illum

    integer :: i_ill, ix, iy, iz, nz
    integer :: nxfft, nyfft

    CHARACTER(len=255) :: cwd, in
    character(len=4)::tag

    double precision :: rms, normef, normeg

    integer :: imax, jmax
    character(8)  :: date
    character(10) :: time
    character( len=2) :: type_GC
    integer,dimension(8) :: values
    
    nxfft = params_direct%Nx
    nyfft = params_direct%Nx
    nz = params_direct%Nz

!    allocate( rhon( nx, ny, nz ))
    allocate( rhon1( nx, ny, nz ))
    allocate( residu( nx, ny, params_direct%nb_illum ))

    allocate( grad( nx, ny, nz ) )
    allocate( gradn1( nx, ny, nz ) )

    allocate( dn( nx, ny, nz ))
    allocate( dn1( nx, ny, nz ))
    
    allocate( temp( nx, ny ))
    

    call lit_mesures(mes)

    ! normalement, l'illumination est connue grace aux mesures experimentales
    ! donc on devrait la lire dans un fichier
    ! TODO pour l'instant  on rentre une illumination, avec une forme/algo définie dans le fichier params
    call set_illum( x_grid, y_grid )
    i_ill = 1 ! TODO faut-il travailler sur une nouvelle image qui est la moyenne de toutes les images?

    ! 1/ on se donne une estimation initiale rho_n1 (\rho_0)
#ifdef DEBUG
    write (*, *) " estimation initiale rho_n1 (\rho_0) taille  ", size(rhon1,1), size(rhon1,2)
#endif
    params_direct%rho = 0.0d0
    ! rho est defini sur des plans (x,y) et calculé sur plusieurs z
    do iz = 1, nz
       do i_ill = 1, params_direct%nb_illum ! on prend la moyenne des images
          params_direct%rho(:, :, iz) = params_direct%rho(:, :, iz) +  mes(:, :, i_ill)
       end do
       params_direct%rho(:, :, iz) = params_direct%rho(:, :, iz) / dble(params_direct%nb_illum)
    end do
#ifdef DEBUG
    print *, "on essaie avec un \rho initial nul"
    params_direct%rho(:, :, :) = 0.0d0
    call   sauve_rho(0, 1, params_direct%rho)
    ! on rentre une densite de fluorophores, avec une forme/algo définie dans le fichier params
    !call set_signal( params_direct%algorho, params_direct%rho, x_grid, y_grid, "rho2d" )
#endif

    ! calcul du coeficient de ponderation W
#ifdef DEBUG2
    print *, "on compare le calcul du coeff de ponderation avec deux methodes"
    print *, " norme des mesures avec norm2 (GNU fortran) ", norm2(real(mes(:, :, 1)))
    call norme_fortran(mes,nx,ny,1,normef)
    print *, " norme des mesures avec fonction kamal ", normef
#endif
    W_gamma = 0.0d0
    !residu(:, :) = data(:, :, i_ill) - params_direct%mes(:, :, i_ill)
    do i_ill = 1, params_direct%nb_illum
       W_gamma = W_gamma + norm2(mes(:, :, i_ill))**2
    end do
    W_gamma = 1.0d0 / W_gamma
    print *, __FILE__, __LINE__, ' W_gamma ', W_gamma

        
    do iter = 1, params_inv%Nbitermax
#ifdef DEBUG
       write (*, *) " debut iteration  ", iter
       !print *, "c to continue..."
       !read (*, "(a)"), in
#endif

       ! 2/ on calcule \rho_n I * p ===> data (meme structure que mes):
       !Estimation initiale pour la densite de fluorophores
#ifdef DEBUG
       print *, __FILE__, __LINE__, " calcul de \rho_n I * p  "
#endif
       ! calcul de \rho I * p, résultat dans params_direct%data
       ! I et p sont connus et ne varient pas, seul \rho varie puisqu'il est recalculé à chaque itération
       i_ill = 1 ! TODO on ne traite que la premiere image/illumination?
       call direct(i_ill, fft_psf) ! le resultat est 2D
       ! 3/ residu = mes-data
       ! pour chaque plan et chaque illumination
#ifdef DEBUG
       print *, __FILE__, __LINE__, "iter: ", iter, &
            &" calcul residu = mes - data, data est (\rhon I * p) "
#endif
       fcout = 0.0d0
       ! calcul des residus pour chaque image/illumination
       do i_ill = 1, params_direct%nb_illum
          residu(:, :, i_ill) = mes(:, :, i_ill) - params_direct%data(:, :, i_ill)
#ifdef DEBUG2
          if (iter == 1) then
             call rms_2D(real(residu(:, :, i_ill)), mes(:, :, i_ill), normef, normeg, rms)
             if (rms > MON_ZERO) then
                print *, __FILE__, __LINE__, " erreur residu devrait &
                     &etre egal a mes puisque data est nul  "
                stop 3287
             else
                print *, __FILE__, __LINE__, " rms ", rms
             end if
          end if
#endif
       end do

#ifdef DEBUG2
       call sauve_residu(iter, residu)  ! params_direct est une variable globale beurk!
#endif

       ! calcul de la fonction de cout
       do   i_ill = 1, params_direct%nb_illum
          call norme_l2( residu(:, :, i_ill), params_direct%nx, params_direct%ny, normef )
          fcout = fcout + normef**2
       end do
       fcout = W_gamma * fcout
#ifdef DEBUG
       print *, __FILE__, __LINE__, 'iter ', iter, ' fcout ', fcout
       if (iter == 1) then
          print *, __FILE__, __LINE__, ' fcout ', fcout, ' doit etre&
               & egal a 1 puisque \rho est 0'
       endif
       !STOP 1000
#endif
          print *, __FILE__, __LINE__, "iter: ", iter, " fcout ",&
               & fcout, " / ", params_inv%tol
          call date_and_time(VALUES=values)
          write(2220,'(i4,i2.2,i2.2, x, i2.2,":",i2.2,":",i2.2, x, i4, x, ES23.16)') &
                  values(1), values(2), values(3), values(5), values(6), values(7), &
                  iter, fcout  ! ecriture du resultat de fcout dans un fichier

       ! 4/ calcul gradient g_rho meme structure que rho
       ! g_rho = -A^dag h
#ifdef DEBUG
       print *, __FILE__, __LINE__, "iter: ", iter, " calcul du gradient "
#endif
       grad = 0.0d0
       do i_ill = 1, params_direct%nb_illum
          do iz = 1, nz
             call conv_2D(residu(:, :, i_ill), fft_psf(:, :, iz), temp(:, :))
             temp(:, :) = params_direct%ill(:, :, iz, i_ill) * temp(:, :)
             grad(:, :, iz) = (params_direct%ill(:, :, iz, i_ill) * temp(:, :)) + grad(:, :, iz)
          end do
       end do
       grad = -2.0d0*W_gamma*grad

#ifdef DEBUG
       print *, __FILE__, __LINE__, "iter: ", iter, " maxval gradient ", maxval(grad)
#endif

#ifdef DEBUG2
       ! plan z = iz
       iz = 1
       call sauve_gradient(iter, iz, grad)
#endif

       !================================================================²

          print *, iter, size(params_direct%rho,1),size(params_direct%rho,2),size(params_direct%rho,3)
       ! changement kamal 21/11/22
       if (iter == 1) then
          dn     = grad ! peut-être -grad TODO
       else
          type_GC = 'PR'
          print *, iter, size(params_direct%rho,1),size(params_direct%rho,2),size(params_direct%rho,3)
          call GC_fluo(type_GC, params_direct%rho, rhon1, grad,&
               & gradn1, dn1, dn)
          print *, iter, size(params_direct%rho,1),size(params_direct%rho,2),size(params_direct%rho,3)

       endif
       !================================================================
       
       ! 4'/ calcul gradient regularisante
       ! 5/ calcul d_rho (gradient conjugue)
       ! 6/ calcul coeff polynome
       !je dois calculer gradient todo
       coeff0 = 0.0d0
       coeff1 = 0.0d0
       coeff2 = 0.0d0

       do i_ill = 1, params_direct%nb_illum
          coeff0 = coeff0 + norm2(residu(:, :, i_ill))**2
          do iz = 1, nz
             ! calcul de d_n I * p -> temp
             !================================================================
             !changement kamal 21/11/22 on prend des directions en
             !gradient conjugué et non en gradient simple
             !================================================================

             call conv_2D(dn(:, :, iz) * params_direct%ill(:, :, iz, i_ill), fft_psf(:, :, iz), temp(:, :))
             do iy = 1,ny
                do ix = 1,nx
                   coeff1  = coeff1 -(2.0d0 * residu(ix, iy, i_ill) * temp(ix, iy))
                enddo
             enddo

             coeff2 = coeff2 + norm2(real(temp(:, :)))**2
          end do
       end do
       coeff0 = W_gamma*coeff0
       coeff1 = W_gamma*coeff1
       coeff2 = W_gamma*coeff2
       
       
       
       

       ! 6'/ calcul coeff regularisante
       ! 7/ calcul alpha (minimisation polynome)
       !call minimis(., .,. alpharho)
       alpha =  -0.5d0 * (coeff1/coeff2)
       ! si  positivité solution soit par cardan; soit par numrec TODO

       !Wgamma*(a1 + alpha a2 + alpha**2 a3) doit etre egal au prochain fcout


       ! 8/ mise a jour rho_n= rho_n1 + alpha d_rho

       !do i_ill = 1, params_direct%nb_illum
       do ix =1,  nx
          do iy =1, ny
             do iz=1, nz
                params_direct%rho(ix, iy, iz)     = params_direct%rho(ix, iy, iz) + (alpha * dn(ix, iy, iz))
                rhon1(ix,iy,iz)                   = params_direct%rho(ix, iy, iz)
                gradn1(ix,iy,iz)                  = grad(ix, iy, iz)
                dn1(ix,iy,iz)                     = dn(ix, iy, iz)
             end do
          end do
       end do
#ifdef DEBUG2
       call   sauve_rho(iter, params_direct%rho)
#endif
    end do

    ! on affiche le rho trouvé
    iz = 1
    call   sauve_rho(0, iz, params_direct%rho)

  end subroutine inverse

  subroutine sauve_residu(iter, residu)
    integer, intent(in) :: iter
    double precision, dimension(:,:, :), allocatable, intent(in) :: residu

    integer :: imax, jmax, ix, iy
    integer :: nxfft, nyfft
    character(len=4)::tag

    nxfft = params_fft%Nxfft
    nyfft = params_fft%Nyfft

#ifdef DEBUG
    i_ill = 1
    write(tag,'(I4.4)') iter
    print*, __FILE__, __LINE__, ' on ecrit les donnees dans le fichier ' , &
         & 'residu_' // trim(tag) // '.txt '
    open(unit=20, file= 'residu_' // trim(tag) // '.txt')
    do ix=1,nx
       do iy=1,ny
          write(20,*) ix, iy, residu(ix, iy, i_ill)
       enddo
    enddo
    close(20)
#endif

  end subroutine sauve_residu

  subroutine sauve_rho(iter, iz, rho)
    integer, intent(in) :: iter, iz
    double precision, dimension( :, :, : ), allocatable, intent(in) :: rho

    integer :: imax, jmax, ix, iy
    integer :: nxfft, nyfft
    character(len=4)::tag
    CHARACTER(len=255) :: cwd

    nxfft = params_fft%Nxfft
    nyfft = params_fft%Nyfft

#ifdef DEBUG
#endif
      CALL getcwd(cwd)
      if (iter > 0) then
          write(tag, '(I4.4)') iter
          print*, __FILE__, __LINE__, ' on ecrit rhon dans le fichier ' &
                  // TRIM(cwd) // '/rhon_apres_' // trim(tag) // '.txt'
          open(unit = 11, file = 'rhon_apres_' // trim(tag) // '.txt')
      else
          print*, __FILE__, __LINE__, ' on ecrit rhon dans le fichier ' &
                  // TRIM(cwd) // '/rhon_trouve' // '.txt'
          open(unit = 11, file = 'rhon_trouve' // '.txt')
      end if

    do ix=1,nx
       do iy=1,ny
          write(11,*) ix, iy, iz, real(params_direct%rho(ix, iy, iz))
       enddo
    enddo
    close(11)
    print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
      if (iter > 0) then
    print *, __FILE__, __LINE__, 'splot "'// TRIM(cwd) // '/rhon_apres_' // trim(tag) &
         // '.txt" using 1:2:4'
else
          print *, __FILE__, __LINE__, 'splot "'// TRIM(cwd) // '/rhon_trouve'  &
                  // '.txt" using 1:2:4'
      end if
  end subroutine sauve_rho

  subroutine sauve_gradient(iter, iz, gradient)
    integer, intent(in) :: iter, iz
    double precision, dimension(:,:,:), allocatable, intent(in) :: gradient

    integer :: ix, iy
    integer :: nxfft, nyfft
    character(len=14)::tag
    CHARACTER(len=255) :: cwd

    nxfft = params_fft%Nxfft
    nyfft = params_fft%Nyfft

#ifdef DEBUG
    write(tag,'(I4.4,A1,I4.4)') iter, '_', iz
    CALL getcwd(cwd)
    print*, __FILE__, __LINE__, ' on ecrit gradient apres calculs dans le fichier ' &
         // TRIM(cwd) // '/gradient_' // trim(tag) // '.txt'
    open(unit=11, file= 'gradient_' // trim(tag) // '.txt')
    do ix=1,nx
       do iy=1,ny
          write(11,*) ix, iy, gradient(ix, iy, iz)
       enddo
    enddo
    close(11)
    print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
    print *, __FILE__, __LINE__, 'splot "'// TRIM(cwd) // '/gradient_' // trim(tag) &
         // '.txt" using 1:2:3'
#endif


  end subroutine sauve_gradient
end program fluo_inverse
