!  Ce code calcule l'image de fluorescence a partir de la densite de
!  fluorophores, l'intensite dans le domaine des fluorophores et la
!  psf du microscope. La convolution est calcule a l'aide des FFT pour
!  les fonctions reelles. Les FFT sont dans ce cas hermitiennes.
!
! Kamal BELKEBIR (novembre 2019)
!
program conv_fluo
  implicit none

  integer, parameter :: nxm=64, nym=64, nxfft = 2*nxm, nyfft=2*nym
  integer :: nx, ny, ix, iy, i, j

  !  f et g sont les fonctions reelles (input); et h est aussi reele
  !  (output). h = f conv g.
  !
  double precision :: f(nxm,nym), g(nxm, nym), h(nxm, nym)

  !  des parametres juste pour l'exemple
  !
  double precision :: sigma, pi, twopi, x(nxm), y(nym), r, lx, ly, l
  double precision :: x_double(nxfft), y_double(nyfft), l2, deltax2
  double precision :: x0, y0, kx(nxfft), ky(nyfft)
  double precision :: a
  double precision :: deltax, deltay
  double precision :: deltakx, deltaky
  double complex, parameter :: ucomp=(1.0d0, 0.0d0)
  double complex, parameter :: icomp=(0.0d0, 1.0d0)

  ! tableaux de travail pour les fft de taille double
  !
  double complex   :: f2(nxfft,nyfft), g2(nxfft,nyfft), h2(nxfft,nyfft)
  double complex   :: conv_analy(nxfft,nyfft)
  double complex   :: zf2(nxfft,nyfft), zg2(nxfft,nyfft), zh2(nxfft,nyfft)
  double complex   :: ztemp

  double complex   :: f_1d(nxm),  fft_1d(nxfft), zfft_f_1D(nxfft)

  integer(8)       :: plan_1df, plan_1db
  integer(8)       :: plan2f, plan2b
  integer          :: iret, omp_get_max_threads ! pour FFT en
  ! parallelisation
  integer          :: fftw_forward, fftw_estimate, fftw_backward

  !  DEBUT
  ! ------
  !  determine le nb de procs pour la parallelisation et
  !  initialisation des plans pour les FFT. planf pour FORWARD et
  !  planb pour BACKWARD.

  !  RMQ: ces initialisations devront etre faites

  !  dans le programme appelant
  !
  call dfftw_init_threads(iret)
  call dfftw_plan_with_nthreads(omp_get_max_threads())

  !  ces parametres sont consistants avec fluotomofwd
  !
  fftw_forward =-1 !  definit la FFT direct
  fftw_backward=+1 !  definit la FFT inverse
  fftw_estimate=64 !  choix de l'algorithme pour les plans

  !
  print*,'debut initialisation des plans'
  call dfftw_plan_dft_2d(plan2b, nxfft, nyfft, f2, zf2,&
       & fftw_backward, fftw_estimate)

  call dfftw_plan_dft_2d(plan2f,nxfft, nyfft, f2, zf2,&
       & fftw_forward, fftw_estimate)
  print*,'fin initialisation des plans'

  call dfftw_plan_dft_1d(plan_1db, nxfft, fft_1d, zfft_f_1d,&
       & fftw_backward, fftw_estimate)

  call dfftw_plan_dft_1d(plan_1df, nxfft, fft_1d, zfft_f_1d,&
       & fftw_forward, fftw_estimate)

  nx=64
  ny=64

  pi=acos(-1.0d0)
  twopi=2.0d0*pi
  sigma=1.0d0
  x0=0.0d0
  y0=0.0d0

  ! echantillonage dans l'espace direct
  !
  l=10.0d0
  lx=l
  ly=l
  deltax=lx/dble(nx)
  deltay=ly/dble(ny)

  ! RMQ : le centre est en (nx/2+1, ny/2+1)
  !
  print*,'calcul des points echantillonage dans l espace direct'
  do ix = 1,nx
     x(ix)=-lx/2.0d0+dble(ix-1)*deltax
  enddo
  do iy = 1,ny
     y(iy)=-ly/2.0d0+dble(iy-1)*deltay
  enddo



  !  echantillonage dans l'espace de FOURIER RMQ : la frequence
  !  spatiale nulle est en nfft/2+1. Les librairies permettant de
  !  calculer les FFT considèrent des supports positifs. Faut tenir
  !  compte de cela.
  !
  deltakx=twopi/(deltax*dble(nxfft))
  deltaky=twopi/(deltay*dble(nyfft))
  do ix=-nxfft/2, nxfft/2-1
     i=ix+nxfft/2+1
     kx(i)=dble(ix)*deltakx
  enddo
  do iy=-nyfft/2, nyfft/2-1
     j=iy+nyfft/2+1
     ky(j)=dble(iy)*deltaky
  enddo


  !  deux gaussiennes identiques centrees de largeur sigma Ceci est
  !  juste un exemple. f representera rho*I et g la psf.
  !
  !  but: on souhaite calculer (f=rho*I) convoluee avec g=psf
  !

  ! definition de deux gaussiennes 2D d'ecart type sigma dont on veut
  ! calculer la convolution
  !
  a=1.0d0/(2.0d0*sigma*sigma)
  do ix=1,nx
     do iy=1,ny
        r= (x(ix)-x0)**2+(y(iy)-y0)**2
        r= r*a
        f(ix,iy) = exp(-r)*(1.0d0/(sigma*sigma*twopi))   
        g(ix,iy) = exp(-r)*(1.0d0/(sigma*sigma*twopi))   
     enddo
  enddo

  !  une gaussienne 1D d'ecart type sigma pour montrer comment marche
  !  la FFT. On calcule la FFT numerique et on compare avec
  !  l'analytique. On passera ensuite au cas 2D et la convolution
  !  proprement dit.
  !
  do ix=1,nx
     r= (x(ix)-x0)**2
     r= r*a
     f_1d(ix)= exp(-r)*(1.0d0/(sigma*sqrt(twopi)))     
  enddo

  !  sauvegarde de la guassienne echantillonnee en nx point
  !
  open(unit=11, file='gauss_1d.mat')
  do ix=1,nx
     write(11,*) x(ix), real(f_1d(ix))
  enddo
  close(11)

  !  On double le tableau. On periodise avec un zero-padding
  !
  fft_1d=cmplx(0.0d0,0.0d0)
  do ix=1,nx/2
     fft_1d(ix)=f_1d(nx/2+ix)
     fft_1d(nxfft-ix+1)=f_1d(nx/2-ix+1)
  enddo

  !  sauvegarde du tableau double periodise et avec le zero-padding
  !
  open(unit=11, file='gauss_1d_double.mat')
  do ix=1,nxfft
     write(11,*) real(fft_1d(ix))
  enddo
  close(11)

  !  calcul de TF numerique de la gaussienne. Le resultat est dans zfft_f_1
  !
  call dfftw_execute_dft(plan_1df, fft_1d, zfft_f_1d)

  !  on remet dans l'ordre (re-periodisation)
  !
  fft_1d=zfft_f_1d
  do ix=1,nxfft/2
     zfft_f_1d(ix)=fft_1d(nxfft/2+ix)
     zfft_f_1d(nxfft-ix+1)=fft_1d(nxfft/2-ix+1)
  enddo
  zfft_f_1d=zfft_f_1d*deltax 

  !  On sauvegrade les parties reellles et imaginaires de la TF
  !  numerique et l'analytique pour pouvoir les tracer sous octave,
  !  gnuplot ou matlab.
  !
  !  RMQ: comme la gaussienne est reelle paire la partie imaginaire
  !  de la sa TF est nulle. De meme qu'une fonction reelle impaire a
  !  sa TF purement imaginaire.
  !
  open(unit=11, file='rgauss_1d_double.mat')
  open(unit=12, file='igauss_1d_double.mat')
  do ix=1,nxfft
     write(11,*) kx(ix),real(zfft_f_1d(ix)), exp(-(kx(ix)&
          &*kx(ix))/(4.0d0*a))
     write(12,*) kx(ix),aimag(zfft_f_1d(ix)), 0.0d0
  enddo
  close(11)


  !  pour la convolution on calcule les tf des deux fonctions, on les
  !  multiplie dans FOURIER puis en revient dans l'espace
  !  direct. Pour le calcul des tf on va suivre la meme demarche
  !  qu'en 1D. Le reste est trivial !

  !  on sauvegarde les signaux originaux de l'espace direct. Deja
  !  calcules plus haut
  !
  open(unit =11, file = 'gauss_f_2d.mat')
  open(unit =12, file = 'gauss_g_2d.mat')
  do iy=1,ny
     do ix=1,nx
        write(11,*) x(ix), y(iy), f(ix,iy)
        write(12,*) x(ix), y(iy), g(ix,iy)
     enddo
  enddo
  close(11)
  close(12)


  !  on double les tableaux en periodisant et avec un zero-padding
  !

  do ix=1,nxfft
     do iy=1,nyfft
        f2(ix,iy)=0.0d0  !  initialise f2 
        g2(ix,iy)=0.0d0  !  initialise g2
     enddo
  enddo


  do ix =1, nx/2
     do iy=1, ny/2
        !  la fonction f
        !
        f2(ix,iy)=f(nx/2+ix,ny/2+iy)
        f2(nxfft-ix+1,nyfft-iy+1)=f(nx/2-ix+1,ny/2-iy+1)
        f2(ix,nyfft-iy+1)=f(nx/2+ix,ny/2-iy+1)
        f2(nxfft-ix+1,iy)=f(nx/2-ix+1,ny/2+iy)

        !  la fonction g
        !
        g2(ix,iy)=g(nx/2+ix,ny/2+iy)
        g2(nxfft-ix+1,nyfft-iy+1)=g(nx/2-ix+1,ny/2-iy+1)
        g2(ix,nyfft-iy+1)=g(nx/2+ix,ny/2-iy+1)
        g2(nxfft-ix+1,iy)=g(nx/2-ix+1,ny/2+iy)
     enddo
  enddo

  !  sauvegarde des fonctions periodisees
  !
  l2=2.0*l
  deltax2=2.0*l2/dble(nxfft)
  do ix=1,nxfft
     x_double(ix)=-l+dble(ix-1)*deltax2
  enddo
  do iy=1,nyfft
     y_double(iy)=-l+dble(iy-1)*deltax2
  enddo
  open(unit =11, file = 'gauss_f_2d2.mat')
  open(unit =12, file = 'gauss_g_2d2.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) x_double(ix), y_double(iy), real(f2(ix,iy))
        write(12,*) x_double(ix), y_double(iy), real(g2(ix,iy))
     enddo
  enddo
  close(11)
  close(12)

  !  calcul des fft de f et g
  !
  print*,'debut de la convolution'
  call dfftw_execute_dft(plan2f, f2, zf2)
  call dfftw_execute_dft(plan2f, g2, zg2)
  ! sauvegarde des fft de f et g
  open(unit =11, file = 'real_fft_f_2d2.mat')
  open(unit =12, file = 'imag_fft_f_2d2.mat')
  do iy=1,nyfft
      do ix=1,nxfft
          write(11,*) kx(ix), ky(iy), real(zf2(ix,iy))
          write(12,*) kx(ix), ky(iy), imag(zf2(ix,iy))
      enddo
  enddo
  close(11)
  close(12)

  !  produit des fft
  !
  do ix=1,nxfft
     do iy=1,nyfft
        zh2(ix,iy)= zf2(ix,iy)*zg2(ix,iy)
     enddo
  enddo

  !  calcul de la fft inverse du produit (convolution dans l'espace
  !  direct)
  print*,'calcul de la fft inverse du produit'
  call dfftw_execute_dft(plan2b, zh2, h2)
  
  write (*,*) " ecriture fichier prod conv partie reelle "
  open(unit=11, file ='raw_real_h2.mat')
  write (*,*) " ecriture fichier prod conv partie reelle "
  open(unit=12, file ='raw_imag_h2.mat')
  do iy=1,nyfft
      do ix=1,nxfft
          write(11,*) kx(ix), ky(iy), real(h2(ix,iy))
          write(12,*) kx(ix), ky(iy), aimag(h2(ix,iy))
      enddo
  enddo
  close(11)
  close(12)

  zh2=h2/dble(nxfft*nyfft)

  !  on periodise a nouveau pour remettre dans l'ordre
  !

  do ix=1,nxfft/2
     do iy=1,nyfft/2
        h2(ix,iy)=zh2(nxfft/2+ix,nyfft/2+iy)

        h2(nxfft-ix+1,nyfft-iy+1)=zh2(nxfft/2-ix+1,nyfft/2-iy+1)
        h2(ix,nyfft-iy+1)=zh2(nxfft/2+ix,nyfft/2-iy+1)
        h2(nxfft-ix+1,iy)=zh2(nxfft/2-ix+1,nyfft/2+iy)
     enddo
  enddo
  open(unit=11, file ='real_conv2d.mat')
  open(unit=12, file ='imag_conv2d.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) kx(ix), ky(iy), real(h2(ix,iy))
        write(12,*) kx(ix), ky(iy), aimag(h2(ix,iy))
     enddo
  enddo
  close(11)
  close(12)

  sigma=2.0*sigma
  a=1.0d0/(2.0d0*sigma*sigma)
  do ix=1,nxfft
     do iy=1,nyfft
        r= (x_double(ix)-x0)**2+(y_double(iy)-y0)**2
        r= r*a
        conv_analy(ix,iy) = exp(-r)*(1.0d0/(sigma*sigma*twopi))   
     enddo
  enddo
  open(unit=11, file ='real_conv2d_analy.mat')
  open(unit=12, file ='imag_conv2d_analy.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) x_double(ix), y_double(iy), real(conv_analy(ix,iy))
        write(12,*) x_double(ix), y_double(iy), aimag(conv_analy(ix,iy))
     enddo
  enddo
  close(11)
  close(12)


  ! on libere les plans
  !
  call dfftw_destroy_plan(plan_1df)
  call dfftw_destroy_plan(plan_1db)
  call dfftw_destroy_plan(plan2f)
  call dfftw_destroy_plan(plan2b)


  !  FIN
  ! ------
end program conv_fluo
