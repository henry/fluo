! voir aussi https://gist.github.com/appleparan/c048c44668ede7ef28ba63c660b6dcf3
module  mod_conv2d
  use mod_params
  use mod_fft

  implicit none

contains

  ! calcul du produit de convolution dans le plan 2D (x,y) pour z fixé
  subroutine conv_2D(op1, fft_psf, res )
    use mod_fft, only : fftshift
    implicit none
    ! calcul de C = u * v
    ! entrées:
    !     op1: premier operande: u
    !     v: second operande, mais ici on a directe sa TF, la fft de la psf: fft_psf
    ! sorties:
    !     res: produit de convolution

    double precision, dimension(:,:), intent(in) :: op1
    double complex, dimension(:,:), intent(in)   :: fft_psf
    !double precision, intent(inout) :: res(:,:)
    double precision, dimension(:,:), intent(inout) :: res
    
    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: ztemp, ztemp2, h2
    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: h   ! resultat du produit de convolution, support double
    integer :: ix, iy

    integer(8)       :: plan2f, plan2b
    integer :: nx, ny, nz, nxfft, nyfft

    nx      = params_direct%Nx
    ny      = params_direct%Ny
    nz      = params_direct%Nz

    allocate( h( nx, ny ))

    nxfft = params_fft%Nxfft
    nyfft = params_fft%Nyfft
    allocate( ztemp(  nxfft, nyfft )  )
    ztemp = 0.0d0
    allocate( ztemp2(  nxfft, nyfft )  )
    ztemp2 = 0.0d0
    allocate( h2( nxfft, nyfft ))
    h2 = 0.0d0

    res( :, : ) = 0.0d0
    call dfftw_plan_dft_2d(plan2f,  nxfft, nyfft, ztemp, ztemp2, &
         FFTW_FORWARD ,FFTW_ESTIMATE)
    call dfftw_plan_dft_2d(plan2b,  nxfft, nyfft, ztemp, ztemp2, &
         FFTW_FORWARD ,FFTW_ESTIMATE)

    ztemp((nxfft-nx)/2:(nxfft+nx)/2-1, (nxfft-nx)/2:(nxfft+nx)/2-1) = op1( :, : )
    call dfftw_execute_dft ( plan2f , ztemp, ztemp2 )
    ztemp = ztemp2 * fft_psf(:, :)  ! produits des fft
    call dfftw_execute_dft(plan2b, ztemp, ztemp2)
    ztemp = ztemp2/dble(nxfft*nyfft)
    call fftshift(ztemp, h2) !  on periodise a nouveau pour remettre dans l'ordre
    h( :,: ) = h2((nxfft-nx)/2:(nxfft+nx)/2-1, (nxfft-nx)/2:(nxfft+nx)/2-1)
    
    res( :, : ) = res(:, :) + real(h(:, :)) * (params_direct%lx/nx) * (params_direct%ly/ny)
    !

    deallocate(ztemp)
    deallocate(ztemp2)
    deallocate( h )
  end subroutine conv_2D
end module mod_conv2d
