!  Ce code calcule l'image de fluorescence a partir de la densite de
!  fluorophores, l'intensite dans le domaine des fluorophores et la
!  psf du microscope. La convolution est calculee a l'aide des FFT pour
!  les fonctions reelles. Les FFT sont dans ce cas hermitiennes.
!
! Kamal BELKEBIR (novembre 2019)
! Gérard HENRY (mai 2021)
!
program fluo_direct

  use FFTW3
  use mod_maths
  use mod_params  !! pour lire dans un fichier les paramètres de la simu
  use mod_utils    !! contient les routines pour la discretisation dans l'espace direct et de Fourier
  use iso_fortran_env
  use, intrinsic :: iso_c_binding
  use mod_conv2d
  use mod_fft, only : fftshift
  use mod_fluo_direct

  implicit none

  !=======================================================================
  ! domaine d'investigation Omega restreint a un plan
  integer :: nx, ny, nz
  !=======================================================================
  double complex, dimension(:,:,:), allocatable   :: fft_psf
  !=======================================================================
  ! variables locales
  integer          :: ix, iy, iz
  integer           :: i_ill

  double precision, dimension(:), allocatable :: x_grid, y_grid
  complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: ztemp, ztemp2

  integer :: nxfft, nyfft
  integer          :: iret, omp_get_max_threads ! pour FFT en
  integer(8)       :: plan2f, plan2b

  ! le programme doit être lancé avec un argument: ./monprog mon-fichier-de-parametres
  ! si l' argument n existe pas on sort
  IF( COMMAND_ARGUMENT_COUNT() /= 1 )THEN
     WRITE(*, *)'ERROR, ONE COMMAND-LINE ARGUMENTS REQUIRED, STOPPING'
     WRITE(*, *)'Usage: ./fluo_direct  ../fluo_inverse.IN'
     STOP 1
  ENDIF

#ifdef DEBUG
  print*, __FILE__, __LINE__, 'This file was compiled by ', &
       compiler_version(), ' using the the options ', &
       compiler_options()
#endif

  call lecture_params()
#ifdef DEBUG
  call affiche_params()
#endif

  nx      = params_direct%Nx
  ny      = params_direct%Ny
  nz      = params_direct%Nz

  allocate( x_grid( nx ) )
  allocate( y_grid( ny ) )

  ! calcul les points de discretisation dans l'espace direct
  call maille_xy( x_grid, y_grid )

  allocate( params_direct%rho( nx, ny, nz ) )
  allocate( params_direct%ill( nx, ny, nz, params_direct%nb_illum ) )
  allocate( params_direct%psf( nx, ny, nz ) )

  nxfft = params_fft%Nxfft
  nyfft = params_fft%Nyfft
  allocate( fft_psf( nxfft, nyfft, nz )  )
  allocate( ztemp(  nxfft, nyfft )  )
  ztemp = 0.0d0
  allocate( ztemp2(  nxfft, nyfft )  )
  ztemp2 = 0.0d0

  allocate( params_direct%data( nx, ny, params_direct%nb_illum ) )

  ! on rentre une densite de fluorophores, avec une forme/algo définie dans le fichier params
  call set_signal( params_direct%algorho, params_direct%rho, x_grid, y_grid, "rho2d" )

  ! on rentre une illumination
  call set_illum( x_grid, y_grid )

  ! init de la lib FFTW3
  call dfftw_init_threads(iret)
  call dfftw_plan_with_nthreads(omp_get_max_threads())

  ! TODO les plans devraient aussi dépendre de z
  do iz = 1, nz
     call dfftw_plan_dft_2d(plan2f,  nxfft, nyfft, ztemp, ztemp2, &
          FFTW_FORWARD ,FFTW_ESTIMATE)
     call dfftw_plan_dft_2d(plan2b,  nxfft, nyfft, ztemp, ztemp2, &
          FFTW_FORWARD ,FFTW_ESTIMATE)
  end do
!!! on fixe la PSF: params_direct%psf , avec une forme/algo définie dans le fichier params
  call set_signal( params_direct%algopsf, params_direct%psf, x_grid, y_grid, "psf2d" )
#ifdef DEBUG
  do ix=1,nx
     do iy=1,ny
        write(1111,*) ix, iy, params_direct%psf(ix, iy, 1)
     enddo
  enddo
#endif
  ! on calcule les FFT de la PSF pour chaque plan z
  do iz = 1, nz
     ztemp((nxfft-nx)/2:(nxfft+nx)/2-1, (nxfft-nx)/2:(nxfft+nx)/2-1) = params_direct%psf(1:nx, 1:ny, iz)
     call dfftw_execute_dft ( plan2f, ztemp, ztemp2 )
     fft_psf(:, :, iz) = ztemp2(:, :)
  enddo

  ! probleme direct
  print*, "**********************************"
  print*, "Debut probleme direct"
  print*, "**********************************"

  ! ici une seule illumnation/image
  print*, "TODO manque la boucle sur les illuminations"
  ! do i_ill = 1, params_direct%nb_illum
  i_ill = 1
  call direct(i_ill, fft_psf) ! calcul de M = \rho I * p, résultat dans params_direct%data

  ! on sauve le resultat obtenu
  call save_data()
  
  deallocate( x_grid )
  deallocate( y_grid)
  deallocate( params_direct%psf )
  deallocate( fft_psf )
  !  FIN
  ! ------


  
end program fluo_direct
