module mod_cte_math
  implicit none


  double precision, parameter :: pi=acos(-1.0d0) 
  double precision, parameter :: twopi=2.0d0*pi
  
  ! Gaussienne
  double precision, parameter :: sigma=1.0d0
  double precision, parameter :: x0=0.0d0, y0=0.0d0

  ! echantillonage dans l'espace direct
  !
  double precision, parameter :: l=10.0d0

end module mod_cte_math
