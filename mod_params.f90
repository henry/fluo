! module de lecture des parametres de la microscopie de fluorescence 3D
! Henry GERARD
! Kamal BELKEBIR (juin 2021)
!
! params est de type stucture contenant les parametres du probleme :
!
! wr         : drapeau d'ecriture
! nb_illum   : nombre d'illuminations
! file_rho   : fichier de la densite de fluorophores
! file_illum : fichier des illuminations
! file_psf   : fichier de la psf
! file_mes.  : fichier des images de fluorescence
! Nx         : dimension suivant  x
! Ny         : dimension suivant  y
! Nz         : dimension suivant  z
! lx
! ly
! lz
! nb_iter
! Nxc
! Nyc
! Nzc
! Nxfft
! Nyfft
! tol
! deltax
! deltay

module  mod_params
  implicit none

  integer, parameter :: LENGTH = 255  ! longueur noms de fichiers

  type PARAMETRES_FLUO_DIRECT
     logical                 :: wr    ! TRUE si écriture des champs dans des fichiers texte

     character(len = :), allocatable :: file_rho
     character(len = :), allocatable :: file_illum
     character(len = :), allocatable :: file_psf
     character(len = :), allocatable :: file_measures

     integer                 :: nb_illum
     integer                 :: Nx
     integer                 :: Ny
     integer                 :: Nz
     integer                 :: Nxcamera
     integer                 :: Nycamera

     double precision        :: lx
     double precision        :: ly
     double precision        :: lz
     double precision        :: sizepixelx
     double precision        :: sizepixely
     double precision        :: lambdafluo

     double precision        :: deltax
     double precision        :: deltay

     integer                 ::  algorho, algopsf  ! voir la déclaration plus bas

     double precision, dimension( :, :, :, : ), allocatable :: ill ! x,y,z, i_ill
     double precision, dimension( :, :, : ), allocatable :: rho ! x,y,z
     double precision, dimension( :, :, : ), allocatable :: psf ! x,y,z
     double precision, dimension( :, :, : ), allocatable :: data ! x,y, i_ill

  end type PARAMETRES_FLUO_DIRECT

  type PARAMETRES_FLUO_INVERSE
     integer                 :: Nbitermax
     double precision        :: tol
  end type PARAMETRES_FLUO_INVERSE

  type PARAMETRES_FFT
     integer                 :: Nxfft
     integer                 :: Nyfft
  end type PARAMETRES_FFT

  type(PARAMETRES_FLUO_DIRECT) :: params_direct
  type(PARAMETRES_FLUO_INVERSE) :: params_inv
  type(PARAMETRES_FFT) :: params_fft


  INTEGER(SELECTED_INT_KIND(4)), PARAMETER :: ONE = 0, PORTE = 1, GAUSS = 2, BESSEL = 3, FILE = 4
  CHARACTER(len=*), dimension(4), parameter :: noms_algos=(/'One   ', 'Porte ', 'Gauss ', 'Bessel'/)

  doubleprecision, parameter :: MON_ZERO = 1e-12 ! a ajuster en fonction des besoins de precision

contains

  !  ici, on lit les arguments de la ligne de commandes, le nom du fichier de paramètres doit s'y trouver
  ! après le contenu du fichier est lu grâce à des NAMELIST
  subroutine lecture_params()
    implicit none

    character(:), allocatable     :: arg
    integer                       :: arglen, stat
    integer                       :: Nx
    integer                       :: Ny
    integer                       :: Nz
    integer                       :: n_illum
    double precision              :: lx
    double precision              :: ly
    double precision              :: lz

    logical :: file_exists, write_to_files
    character(len = LENGTH)  :: message

    character(len = LENGTH) :: densite
    character(len = LENGTH) :: illumination
    character(len = LENGTH) :: psf
    character(len = LENGTH) :: measures
    integer :: nb_iter
    double precision :: tol
    integer :: Nxc, Nyc
    integer :: Nxfft, Nyfft
    character(len = LENGTH) :: algo_rho, algo_psf
    ! les listes
    namelist /geometrieXYZ/  lx, ly, lz, Nx, Ny, Nz, algo_psf
    namelist /illuminations/ n_illum
    namelist /fichiers/      write_to_files, densite, illumination, psf, measures
    namelist /iterations/    nb_iter, tol
    namelist /capteur/       Nxc, Nyc
    namelist /fft/           Nxfft, Nyfft

    ! valeurs par défaut
    params_direct%wr            = .true.
    params_direct%nb_illum      = 1
    !    params_direct%file_rho      = "rho.txt"
    !    params_direct%file_illum    = "illum.txt"
    !    params_direct%file_psf      = "psf.txt"
    !    params_direct%file_measures = "measures.txt"
    params_direct%Nx            = 4
    params_direct%Ny            = 4
    params_direct%Nz            = 4
    params_direct%lx            = 4
    params_direct%ly            = 4
    params_direct%lz            = 1

    call get_command_argument(number = 1, length = arglen)  ! Assume for simplicity success
    allocate (character(arglen) :: arg)
    call get_command_argument(number = 1, value = arg, status = stat)

#ifdef DEBUG
    print*, __FILE__, __LINE__,' lecture du fichier ', arg
#endif
    open (unit = 99, file = arg, status = 'old', action = 'read')
    !print *, "nb de plans en z"
    read(99, nml = geometrieXYZ)
    params_direct%lx = lx; params_direct%ly = ly; params_direct%lz = lz
    params_direct%Nx = Nx; params_direct%Ny = Ny; params_direct%Nz = Nz
    
    read(99, nml = illuminations)
    params_direct%nb_illum = n_illum
    
    read(99, nml = fichiers)
    params_direct%wr = write_to_files
    params_direct%file_rho = trim(densite)
    params_direct%file_illum = trim(illumination)
    params_direct%file_psf = trim(psf)
    params_direct%file_measures = trim(measures)
    
    read(99, nml = iterations)
    params_inv%Nbitermax = nb_iter
    params_inv%tol = tol
    
    read(99, nml = capteur)
    params_direct%Nxcamera = Nxc
    params_direct%Nycamera = Nyc
    if ((params_direct%Nxcamera /= params_direct%Nx) .or. (params_direct%Nycamera /= params_direct%Ny)) then
       print *, __FILE__, __LINE__, " Warning: pour l'instant, (Nx,Ny) &
            &et (Nxcamera, Nycamera) ne doivent pas differer"
       print*, __FILE__, __LINE__, ' on force (Nxcamera, Nycamera) idem (Nx,Ny) '
       params_direct%Nxcamera = params_direct%Nx
       params_direct%Nycamera = params_direct%Ny
    end if
    read(99, nml = fft)
    params_fft%Nxfft = Nxfft
    params_fft%Nyfft = Nyfft
      ! pour pouvoir faire du zero-padding pour des petitis Nx,Ny, il faut que:
      ! Nxfft > Nx
    if ((Nxfft <= Nx) .or. (Nyfft <= Ny)) then
       print *, __FILE__, __LINE__, " Error: (Nxfft,Nyfft) ", Nxfft, Nyfft, &
               " doivent être plus grands que (Nx, Ny)", Nx, Ny , &
       " prendre des multiples de 2! "
#ifdef DEBUG2
       print*, __FILE__, __LINE__, ' on force (Nxfft,Nyfft) idem (Nx,Ny) '
       params_fft%Nxfft = params_direct%Nx
       params_fft%Nyfft = params_direct%Ny
#else
       STOP 226
#endif
    end if

    close(99)

    ! echantillonage dans l'espace direct
    !
    params_direct%deltax=params_direct%lx/dble(params_direct%Nx)
    params_direct%deltay=params_direct%ly/dble(params_direct%Ny)

    ! récupération du type de signal pour \rho, psf et illum
    ! One, Porte, Gaussienne, Bessel
    select case (params_direct%file_rho)
    case ("One")
       params_direct%algorho = ONE
    case ("Porte")
       params_direct%algorho = PORTE
    case ("Gaussienne")
       params_direct%algorho = GAUSS
    case ("Bessel")
       params_direct%algorho = BESSEL
    case default
       INQUIRE(FILE=params_direct%file_rho, EXIST=file_exists)
       if (file_exists) then
          params_direct%algorho = FILE
          message = "traitement du fichier " // params_direct%file_rho // " ! "
          print *, __FILE__, __LINE__, trim(message)
       else
          message = "TODO absence fichier densite ! "
          print *, __FILE__, __LINE__, trim(message)
          STOP 225
       end if
    end select
    select case (params_direct%file_psf)
    case ("One")
       params_direct%algopsf = ONE
    case ("Porte")
       params_direct%algopsf = PORTE
    case ("Gaussienne")
       params_direct%algopsf = GAUSS
    case ("Bessel")
       params_direct%algopsf = BESSEL
    case default
       INQUIRE(FILE=params_direct%file_psf, EXIST=file_exists)
       if (file_exists) then
          params_direct%algopsf = FILE
          message = "traitement du fichier " // params_direct%file_psf // " ! "
          print *, __FILE__, __LINE__, trim(message)
       else
          message = "TODO absence fichier PSF ! "
          print *, __FILE__, __LINE__, trim(message)
          STOP 228
       end if
    end select
    deallocate(arg)
  end subroutine lecture_params

  subroutine affiche_params()

#ifdef DEBUG
    print*, __FILE__, __LINE__, ' affiche_params '
#endif
    print*, "**********************************"
    print*, "Parametres pour le probleme direct"
    print*, "**********************************"
    print*, "Dimensions Domaine Omega (nm) lx ", params_direct%lx, " ly ", params_direct%ly, " lz", params_direct%lz
    print*, "Domaine Omega Nx ", params_direct%Nx, " Ny ", params_direct%Ny, " Nz", params_direct%Nz
    print*, "Echantillonage: dx ", params_direct%deltax, " dy ", params_direct%deltay

    print*, "Densité (\rho): ", params_direct%file_rho
    print*, "Illumination: ", params_direct%file_illum
    print*, "PSF: ", params_direct%file_psf

    print*, "Pixels Camera Domaine Gamma Nxc ", params_direct%Nxcamera, " Nyc ", params_direct%Nycamera
    print*, "Measures: ", params_direct%file_measures

    print*, "**********************************"
    print*, "Parametres pour le probleme inverse"
    print*, "**********************************"
    print*, "pour la resolution: Nbitermax ", params_inv%Nbitermax, &
         &" tol ", params_inv%tol

    print*, "**********************************"
    print*, "Parametres pour la FFT"
    print*, "**********************************"
    print*, "pour la FFT: Nxfft, Nyfft ", params_fft%Nxfft, params_fft%Nyfft

  end subroutine affiche_params

  ! lecture d'un fichier contenant des mesures:
  ! pour l'instant on se contente de relire le fichier cree par le programme fluo_direct
  ! si on doit lire un vrai fichier de mesures, il s'agit d'un TIFF qui contient toutes
  ! les images d'illumination (x,y)*nb_illum
  ! TODO en ne lisant qu'un seul fichier provnant du code fluo_direct, on ne lit qu'une
  ! seule illumination alors qu'il faudrait en lire plusieurs, comme dans un TIFF
  subroutine lit_mesures(data)
    use FFTW3

    double precision, dimension(:,:,:), allocatable, intent(out) :: data

    logical :: file_exists
    character(LENGTH)  :: message
    integer :: i, j, l
    double precision :: val

    allocate( data( params_direct%Nx, params_direct%Ny, params_direct%nb_illum ))

    INQUIRE(FILE=params_direct%file_measures, EXIST=file_exists)
    if ( .not. file_exists) then
       message = "TODO absence fichier mesures ! "
       print *, __FILE__, __LINE__, trim(message)
       STOP 225
    end if
    message = "traitement du fichier " // params_direct%file_measures // "  "
    print *, trim(message)

    open (unit = 99, file = params_direct%file_measures, status = 'old', action = 'read')
    do l=1,params_direct%Nx*params_direct%Ny ! nb de lignes du fichier
       read(99,*) i, j, val
       data(i, j, :) = val
    end do
    close(99)

  end subroutine lit_mesures
    
    subroutine lecture(nom, tab)
        CHARACTER(len=*) :: nom
        double precision, dimension(:,:, :), allocatable, intent(inout)   :: tab
        
        integer :: nlignes
        integer :: i, j, k
        double precision :: x, y, val
        character(LENGTH)  :: message
        
        if (nom == "rho2d") then
            open (unit = 99, file = params_direct%file_rho, form='formatted', status = 'old', action = 'read')
        elseif (nom == "psf2d") then
            open (unit = 99, file = params_direct%file_psf, form='formatted', status = 'old', action = 'read')
        else
            message = "TODO absence fichier densite ! "
            print *, __FILE__, __LINE__, trim(message)
            STOP 325
        end if
        nlignes = 0
        do
            read(99, fmt='(2i4,2f19.15,e25.17)', end=999) i, j, x, y, val
            !read(99, '(2i4,2f8.3,e23.2)', end=999) i, j, x, y, val
            !write (*, '(2i4,2f8.3,e23.2)') i, j, x, y, val
            tab(i, j, :) = val
            nlignes = nlignes + 1
        end do
        999 continue
        print *, '-- fin du fichier'
        close(99)
    
    end subroutine lecture
    
    
    subroutine save_signal(tab, chaine, iz)
        implicit none
        double precision, dimension(:,:,:), allocatable :: tab
        character (*) :: chaine
        integer :: iz
        
        integer :: ix, iy
        CHARACTER(len=255) :: cwd
        character(len=4)::tag
        
    write(tag,'(I4.4)') iz
    print*, __FILE__, __LINE__, 'iz: ', iz, ' on ecrit les donnees dans le fichier ' , &
         & trim(chaine) // "_" // trim(tag) // '.txt '
    open(unit=11, file= trim(chaine) // "_" // trim(tag) // '.txt')
        write(11,*) "#ix iy value"
        do ix=1,size(tab,1)
       do iy=1,size(tab,1)
          write(11,*) ix, iy, tab(ix, iy,iz)
       enddo
    enddo
    close(11)
    CALL getcwd(cwd)
    print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
    print *, __FILE__, __LINE__, 'splot "' // TRIM(cwd) // "/" // trim(chaine) &
         & // "_" // trim(tag) // '.txt" using 1:2:3'
    
    end subroutine save_signal

end module mod_params
