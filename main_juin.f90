program main_juin
  implicit none
  
  integer, parameter         :: LENGTH = 255  ! longueur noms de fichiers
  
  type PARAMETRES_FLUO_DIRECT
     logical                 :: wr    ! TRUE si écriture des champs dans des fichiers texte
     
     character(len = LENGTH) :: file_rho
     character(len = LENGTH) :: file_ill
     character(len = LENGTH) :: file_psf
     character(len = LENGTH) :: file_mes

     integer                 :: nb_ill
     integer                 :: Nx
     integer                 :: Ny
     integer                 :: Nz
     integer                 :: Nxcamera
     integer                 :: Nycamera
     integer                 :: Nxfft
     integer                 :: Nyfft
     
     double precision        :: lx
     double precision        :: ly
     double precision        :: lz
     double precision        :: sizepixelx
     double precision        :: sizepixely
     double precision        :: lambdafluo

     double precision        :: deltax
     double precision        :: deltay

     double precision, dimension( :, :, :, : ), allocatable :: ill
     double precision, dimension( :, :, : ), allocatable :: rho
     double precision, dimension( :, :, : ), allocatable :: psf
     double precision, dimension( :, :, : ), allocatable :: mes

  end type PARAMETRES_FLUO_DIRECT

  type PARAMETRES_FLUO_INVERSE
     integer                 :: Nbitermax
     double precision        :: residu
  end type PARAMETRES_FLUO_INVERSE

  type PARAMETRES_FFT

     integer, parameter ::  FFTW_FORWARD  = -1 
     integer, parameter ::  FFTW_BACKWARD = 1 
     INTEGER, parameter ::  FFTW_ESTIMATE = 64
     
     integer*8 :: plan1f, plan1b
     integer*8 :: plan2f, plan2b
     integer*8 :: plan3f, plan3b
     
  end type PARAMETRES_FFT

  type( PARAMETRES_FLUO_DIRECT  ) :: params_fluo_d
  type( PARAMETRES_FLUO_INVERSE ) :: params_fluo_i
  type( PARAMETRES_FFT )          :: params_FFT

  !=======================================================================
  character( len = LENGTH ) :: file_rho
  character( len = LENGTH ) :: file_ill
  character( len = LENGTH ) :: file_psf
  character( len = LENGTH ) :: file_mes
  !=======================================================================
  integer :: nx, ny, nz
  integer :: nxfft, nyfft
  !=======================================================================
  double precision, dimension( :, :, :, : ), allocatable :: ill
  double precision, dimension( :, :, : ), allocatable :: rho
  double precision, dimension( :, :, : ), allocatable :: psf
  double precision, dimension( :, :, : ), allocatable :: mes
  !=======================================================================
  double complex, dimension( :, :, : ), allocatable :: fftpsf
  !=======================================================================
  ! variables locales
  integer          :: ix, iy, iz
  double precision :: rtemp
  double complex   :: ztemp

  !=======================================================================
  ! parametres pour FFTW 
  integer, parameter ::  FFTW_FORWARD  = -1 
  integer, parameter ::  FFTW_BACKWARD = 1 
  INTEGER, parameter ::  FFTW_ESTIMATE = 64
 
  integer*8 :: plan2f, plan2b
 
  !=======================================================================

  !=======================================================================
  interface
     subroutine fft_psf( psf, fftpsf )
       type( PARAMETRES_FFT_DIRECT) :: params
       double precision, dimension( : , : ) :: psf
       double complex, dimension( :, : )    :: fftpsf
     end subroutine fft_psf

  end interface
  !=======================================================================

  
  !
  file_rho               = "rho.mat"
  file_ill               = "ill.mat"
  file_psf               = "psf.mat"
  file_mes               = "mes.mat"
  !
  nx                     = 64
  ny                     = 64
  nz                     = 16
  !
  nxfft                  = 2*nx
  nyfft                  = 2*ny

  Nb_ill                 = 1
  !
  params_fluo_d%nx       = nx
  params_fluo_d%ny       = ny
  params_fluo_d%nz       = nz
  !
  params_fluo_d%file_rho = file_rho
  params_fluo_d%file_ill = file_ill
  params_fluo_d%file_psf = file_psf
  params_fluo_d%file_mes = file_mes
  !
  allocate( psf( nx, ny, nz ) )
  allocate( rho( nx, ny, nz ) )
  allocate( ill( nx, ny, nz, nb_ill ) )
  !
  allocate( fftpsf( nxfft, nyfft, nz ) ) 
  ! on rentre une densite de fluorophores
  do ix = 1, nx
     do iy = 1, ny
        do iz = 1, nz
           rho( ix, iy, iz ) = 1.0D0
        enddo
     enddo
  enddo
  ! on rentre une illumination
  do i_ill = 1, nb_ill
     do ix = 1, nx
        do iy = 1, ny
           do iz = 1, nz
              ill( ix, iy, iz, i_ill ) = 1.0D0
           enddo
        enddo
     enddo
  enddo

  ! on rentre une psf
  do ix = 1, nx
     do iy = 1, ny
        do iz = 1, nz
           psf( ix, iy, iz ) = 1.0D0
        enddo
     enddo
  enddo
  
  FFTW_FORWARD  = params_FFT%FFTW_FORWARD
  FFTW_BACKWARD = params_FFT%FFTW_BACKWARD
  FFTW_ESTIMATE = params_FFT%FFTW_ESTIMATE
  ! Atention çane marche pas à revoir les arguments
  call dfftw_plan_dft_2d( plan2f, N, f, ft, FFTW_FORWARD, FFTW_ESTIMATE )
  call dfftw_plan_dft_2d( plan2b, N, f, ft, FFTW_BACKWARD, FFTW_ESTIMATE )
  
  do iz = 1, nz
     call fft_psf( params_FFT, psf( :, :, iz), fftpsf( :, :, iz ) )
  enddo

  ! probleme direct
  do i_ill = 1, nb_ill
     mes( :, :, i_ill) = 0.0D0
     do iz = 1, nz
        do ix = 1, nx
           do iy =1, ny
              tab2dtemp( ix, iy ) = rho( ix, iy, iz )*ill( ix, iy, iz, ill)
           enddo
        enddo
        call conv2D(params, tab2dtemp, fftpsf)
        do ix = 1, nx
           do iy = 1, ny
              mes( ix, iy, ill ) = mes(ix, iy, ill) + tab2dtemp(ix, iy)
           enddo
        enddo
     enddo
  enddo
  ! on commence l'inversion
  ! 1/ on se donne une estimation initiale rho_n1
  do iter = 1, nbitermax
     ! 2/ on calcule data (meme structure que mes)
     ! 3/ residu = mes-data
     ! 4/ calcul gradient g_rho meme structure que rho
     ! g_rho = -A^dag h
     ! 4'/ calcul gradient regularisante
     ! 5/ calcul d_rho (gradient conjugue)
     ! 6/ calcul coeff polynome
     ! 6'/ calcul coeff regularisante
     ! 7/ calcul alpha (minimisation polynome)
     ! 8/ mise a jour rho_n= rho_n1 + alpha d_rho
  enddo
  deallocate( psf )
  deallocate( rho )
  deallocate( ill )
  deallocate( fftpsf )
end program main_juin


subroutine fft_psf( params, psf, fftpsf )
  implicit none
  type( PARAMETRES_FFT_DIRECT) :: params
  double precision, dimension( : , : ) :: psf
  double complex, dimension( :, : )    :: fftpsf
  double complex, , allocatable, dimension( :, : )    :: tab2Dtemp
  integer :: ix, iy

  allocate( tab2Dtemp( params%nxfft, params%nyfft ))
  ! mettre psf dnas tba2temp
  do ix = 1, params%nx
     do iy = 1, params%ny
        ! TODO
     enddo
  enddo
  ! on periodise TODO
  call fftw_execute( )

  ! RMQ: on ne periodise pas

  ! TODO mettre le resultat dans fftpsf
  deallocate( tab2Dtemp ) 
  return
end subroutine fft_psf
