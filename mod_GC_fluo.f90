module mod_GC_fluo
    implicit none
    
    interface
        subroutine GC_fluo(type_GC, rho_n, rho_n1, grho_n, grho_n1,&
                & drho_n1, drho_n)
            character( len=2), intent(in)                     :: type_GC
            double precision, dimension(:,:,:), intent(in)    :: rho_n, rho_n1
            double precision, dimension(:,:,:), intent(in)    :: grho_n, grho_n1
            double precision, dimension(:,:,:), intent(in)    :: drho_n1
            double precision, dimension(:,:,:), intent(inout)   :: drho_n
            
        end subroutine GC_fluo
    end interface
end module mod_GC_fluo

!================================================================
!     Calcul de la direction de descente en Gradient Conjugue
!     INPUT
!     -----

!     Type_GC : 'PR' Polak-Ribiere [1-2];
!               'GG' gradient simple;
!               'DU" direction fonction unite;
!               'FR' Fletcher-Reeves [3];
!               'DY' Dai-Yuan [4];
!               'HS' Hestenes-Stiefel [5]. 


!     grho_n  : gradient a l'iteration n
!     grho_n1 : gradient a l'iteration n-1
!     drho_n1 : direction de descente a l'iteration n-1
!     
!
!     OUTPUT
!     ------
!     drho_n : direction de descente a l'iteration n
!
!
!     Refs:
!     [1] E.~Polak, G.~Ribiere, Notes sur la convergence de
!     methodes de directions conjuguees, Revue fraçaise d'informatique
!     et de recherche operationnelle. Serie rouge, tome 3, n° R1 (1969),
!     pp. 35-45. (Polak_RIRO_69).
!
!     [2] B.~T.~Polyak, The conjugate gradient method in extreme problem,
!     USSR Computational Mathematics and Mathematical Physics, 9,
!     (1969), pp. 94-112. (Polyak_USSRCMMP_69).
!
!     [3] R.~Fletcher, C.~M. Reeves, Function minimization by conjugate
!     gradients, The Computational Journal, 7, (1964),
!     pp. 149-154. (Fletcher_CJ_64).
!
!     [4] Y.~H. Dai, Y. Yuan, A nonlinear conjugate gradient method with
!     a strong global convergence proprety, SIAM J. OPTIM., 10, (1999),
!     pp. 177-182. (Dia_SIAM_99).
!
!     [5] M.~R. Hestenes, E.~Stiefel, Methods of conjugate gradients for
!     solving linear systems, J. Res. NIST, 49, (1952),
!     pp.409-436. (Hestenes_JRNIST_52).
!
!     Kamal BELKEBIR, novembre 2022.
!================================================================
      subroutine GC_fluo( type_GC, rho_n, rho_n1, grho_n, grho_n1,&
           & drho_n1, drho_n)
      implicit none
      character( len=2), intent(in)                     :: type_GC
      double precision, dimension(:,:,:), intent(in)    :: rho_n, rho_n1
      double precision, dimension(:,:,:), intent(in)    :: grho_n, grho_n1
      double precision, dimension(:,:,:), intent(in)    :: drho_n1
      double precision, dimension(:,:,:), intent(inout)   :: drho_n
      !================================================================
      !     variables locales
      
      integer          ix, iy, iz, nx, ny, nz
      double precision gamma, ctmp, ctmp1
      !================================================================

      nx = size(rho_n,1)
      ny = size(rho_n,2)
      nz = size(rho_n,3)

     nx = size(drho_n1,1)
      ny = size(drho_n1,2)
      nz = size(drho_n1,3)

#ifdef DEBUG
      print *, __FILE__, __LINE__, " nx ny nz par size() ", &
      & size(rho_n,1), size(rho_n,2), size(rho_n,3)
#endif
      select case ( type_GC )
      case ( 'PR' )
         ctmp1 = 0.0d0
         ctmp  = 0.0d0
         do iz = 1,nz
            do iy = 1, ny
               do ix = 1,nx
                  ctmp1 = ctmp1+abs(grho_n1(ix,iy,iz))**2
                  ctmp  = ctmp+(grho_n(ix,iy,iz))*(grho_n(ix,iy,iz)&
                       &-grho_n1(ix,iy,iz))
               enddo
            enddo
         enddo
         gamma = ctmp/ctmp1
         drho_n = -grho_n+gamma*drho_n1
         !
         !do iz = 1,nz
         !   do iy = 1,ny
         !      do ix = 1, nx
         !         drho_n(i) = -grho_n(i)+gamma*drho_n1(i)
         !      enddo
         !   enddo
         !enddo

!================================================================

      case ( 'FR' ) 
         ctmp1 = 0.0d0
         ctmp  = 0.0d0
         do iz = 1,nz
            do iy = 1, ny
               do ix = 1,nx
                  ctmp1 = ctmp1+abs(grho_n1(ix,iy,iz))**2
                  ctmp  = ctmp+grho_n(ix,iy,iz)*grho_n(ix,iy,iz)
               enddo
            enddo
         enddo
         gamma = ctmp/ctmp1
         drho_n = -grho_n+gamma*drho_n1
      endselect
      return
    end subroutine GC_fluo


