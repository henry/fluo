! Created by me on 27/04/2022.

module mod_coeff_fresnel
    implicit none

contains
    subroutine calcul_coeff_reflexion(theta, zk1, zk2, zn1, zn2, zrpara, ztpara, zrortho, ztortho, beta1, beta2)
        use mod_physi
        implicit none

        double precision, intent(in) :: theta
        double complex, intent(in) :: zk1, zk2
        double complex, intent(in) :: zn1, zn2
        double complex, intent(out) :: zrpara, zrortho
        double complex, intent(out) :: ztpara, ztortho
        double complex, intent(out) :: beta1, beta2
        
        double complex :: alpha1

#ifdef DEBUG1
       print*, __FILE__, __LINE__,' debut '
#endif
        
        alpha1 = zk1 * sin(theta)
        
        beta1 = sqrt(zk1 * zk1 - alpha1 * alpha1)  ! TODO vrai si n1 < n2
        !print*, beta1
        beta1 = zk1 * cos(theta)
        !print*,beta1
        
        beta2 = sqrt(zk2 * zk2 - alpha1 * alpha1)
        
        zrpara = (beta1 - beta2) / (beta1 + beta2)
        ztpara = (2.0D0 * beta1) / (beta1 + beta2)
        
        zrortho = (beta1 - (beta2 / (zn2**2))) / (beta1 + (beta2 / (zn2**2)))
        ztortho = (2.0D0 * beta1) / (beta1 + (beta2 / (zn2**2)))
        !ztortho = zn2 * 2.0d0 / (zn1 + zn2)
    
    endsubroutine calcul_coeff_reflexion


end module mod_coeff_fresnel
