! Created by me on 03/12/2021.

module mod_porte

    implicit none
    
contains
    
    ! porte 2D de largeur e, symetrique dans les deux dimensions
    function porte(x, y, e)
        double precision porte
        double precision, intent(in) :: x, y, e
        
        if ((abs(x) <= 0.5D0 * e) .and. (abs(y) <= 0.5D0 * e))then
            porte = 1.0D0
        else
            porte = 0.0D0
        endif
    
    end function porte


end module mod_porte