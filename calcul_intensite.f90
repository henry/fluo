! Created by me on 27/04/2022.
! il s'agit de calculer l'intensité pour un objet eclairé en fluo dans un domaine \omega
! l'intensité est reçe sur un domaine 2D \gamma
! l'intensité est le module au carré du champ réfléchi
! le champ incident est celui d'une onde plane TE
! le milieu 1 est del'air
! le milieu 2 est le verre
! z est l'axe vertical
! E0 = 1 par convention

program calcul_intensite
    use iso_fortran_env
    use, intrinsic :: iso_c_binding
    use mod_physi
    use mod_coeff_fresnel
    
    implicit none
    
    double precision :: theta, thetad
    double complex :: alpha1
    double complex :: beta1, beta2
    ! les coeffs de Fresnel
    double complex :: zrpara, zrortho
    double complex :: ztpara, ztortho
    
    ! l'intensité == module du champ éectrique au carré
    double precision, allocatable, dimension(:) :: intensite
    integer :: i, iang, nz  ! nb de points en z
    integer :: ithetamax ! max de \theta
    double precision :: z, dz
    
    character (len = *), parameter :: file_intensite = 'intensite.dat'
    character (len = *), parameter :: plt_intensite = 'intensite.plt'
    
    double complex :: zn1, zn2, zk1, zk2
    double precision :: lambda
    parameter (lambda = 500.0d-9) ! longueur d'onde
    double precision :: k0
    parameter (k0 = (2.0D0 * pi / lambda))  ! nombre d'onde dans le vide
    
!    parameter (zn1 = 1.0D0 * ucomp)        ! air
!    parameter (zn2 = sqrt(2.25d0) * ucomp) ! verre
    !
!    parameter (zk1 = k0 * zn1)   ! nombre d'onde dans le milieu n1
!    parameter (zk2 = k0 * zn2)   ! nombre d'onde dans le milieu n2

#ifdef DEBUG1
  print*, __FILE__, __LINE__, 'This file was compiled by ', &
       compiler_version(), ' using the the options ', &
       compiler_options()
#endif
    
    print *, "saisir indice milieu 1 (réel): "
    read *, zn1
    print *, "indice milieu 1: ", zn1
    print *, "saisir indice milieu 2 (réel): "
    read *, zn2
    print *, "indice milieu 2: ", zn2
    zk1 = k0 * zn1
    zk2 = k0 * zn2
    !if (zn1 > zn2) then
    !print *, "angle de refraction limite \theta_l ", asin(real(zn2/zn1)), zn2/zn1
    !endif
    print *, 'saisir un angle en degré: '
    read *, thetad
    print *, 'angle en degré: ', thetad
!#endif
    z = 1! m ! 100. * lambda
    print *, 'saisir distance en z (en mètres): '
    read *, z
    print *, " distance du plan \gamma en z (longueur d'onde:  ", lambda, " m) : ", z, "m "
    nz = 2  ! on prend 4 hauteurs
    print *, 'saisir nombre de points en z: '
    read *,nz
    dz = z / nz
    print *, " pour n = ", nz, " dz: ", dz
    print *, " k1 = ", zk1
    print *, " k2 = ", zk2
    open(unit = 11, file = file_intensite)
    print *, "ouverture du fichier ", file_intensite
    
    allocate(intensite(nz))
    print *, 'saisir \theta max (en deg.) [0-90]: '
    read *, ithetamax
    print *, "on fait varier \theta de 0 à ", ithetamax, " deg."
#ifdef DEBUG1
    print *, "*****************************************"
        print *, "angle     \theta      cos(\theta)"
#endif
    do iang = 0, ithetamax*2
        thetad = dble(iang) * 0.5D0
        theta = thetad * pi / 180.0D0
#ifdef DEBUG1
        print *, iang, thetad, cos(theta)
#endif
        alpha1 = k0 * sin(thetad)
        beta1 = k0 * cos(thetad)
        call calcul_coeff_reflexion(theta, &
                & zk1, zk2, &
                & zn1, zn2, &
                & zrpara, ztpara, zrortho, ztortho, &
                & beta1, beta2)
        !print *, "r// = ", zrpara
        do i = 1, nz
            z = dble(i - 1) * dz
            intensite(i) = 1.
            intensite(i) = 1. + abs(zrpara)**2 + 2.0D0*real(zrpara*exp(2.0d0*icomp*(beta1*z)))
            !print *, " intensite pour angle ", thetad, " et z: ", z
#ifdef DEBUG1
            print*, __FILE__, ":", __LINE__, " " , z,thetad,zrpara, icomp,intensite(i),beta1
            stop
#endif
        enddo
        !print *, " angle ", thetad, " intensité: ", intensite
        ! l'intensité est sauvée à plusieurs hauteurs cad nz hauteurs, commence à z=0
        write(11, *) thetad, intensite
    enddo
    deallocate(intensite)
    close (11)
    open(unit = 12, file = plt_intensite)
    print *, "ouverture du fichier ", plt_intensite
    write(12, *) "set encoding utf8"
    write(12, *) "set terminal epslatex standalone"
    write(12, *) "set output 'intensite_courbe.tex'"
    write(12, *) "set xlabel 'angle'"
    write(12, *) "set ylabel 'intensite'"
    write(12, *) "set grid"
    write(12, *) "#replot"
    write(12, FMT='(A, A, A, f4.2, A, A, F4.2, A, A)') "plot '", file_intensite, "' using 1:2 w lp title ""Intensité pour n1=", &
            &real(zn1), "\n", &
        & " n2=", real(zn2), " \n z=0", """"
    close(12)
    
    print *, "exécuter la commande suivante:"
    print *, "gnuplot -persist ", plt_intensite
end program calcul_intensite