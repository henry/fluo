#!/bin/bash

# script de lancement des simulations
# GH 21 fev 22


# on utilise le code direct pour créer un fichier de mesures
chemin="/home/me/projets/fluo/runs/tests/"

for mytest in "portes" "gaussiennes" "deuxgaussiennesidentiques"
do
    echo $mytest
    cd ${chemin}/${mytest}
    METH="direct"
    MYBIN=fluo_$METH
    ../../../cmake-build-release/$MYBIN fluo_$METH.IN
    if [ $? != 0 ]; then
	echo "Erreur de " $MYBIN
	exit 1
    fi

    METH="inverse"
    MYBIN=fluo_$METH
    ../../../cmake-build-release/$MYBIN fluo_$METH.IN
    if [ $? != 0 ]; then
	echo "Erreur de " $MYBIN
	exit 1
    fi

done

exit

echo "lancement matlab, wait..."
cp ../../../matlab/verifie_fluo_$METH.m verifie_fluo_$METH.m
sed -i 's/^chemin.*/chemin=".\/"/' verifie_fluo_$METH.m
matlab -nodesktop < verifie_fluo_$METH.m
rm verifie_fluo_$METH.m



echo "lancement matlab, wait..."
cp ../../../matlab/verifie_fluo_$METH.m verifie_fluo_$METH.m
sed -i 's/^chemin.*/chemin=".\/"/' verifie_fluo_$METH.m
matlab -nodesktop < verifie_fluo_$METH.m
rm verifie_fluo_$METH.m

