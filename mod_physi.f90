module mod_physi
    implicit none
    
    double complex :: ucomp, icomp
    double precision :: pi
    parameter (ucomp = cmplx(1.0D0, 0.0D0), icomp = cmplx(0.0D0, 1.0D0), pi = acos(-1.0D0))
    
    ! constantes physiques
    double precision c0, eps0, mu0
    parameter (c0 = 299792458.0D0)
    parameter (mu0 = 4.0D-7 * pi)
    parameter (eps0 = 1.0D0 / (c0 * c0 * mu0))
    
end module mod_physi
