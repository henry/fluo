module  mod_fft_param
  implicit none

  integer, parameter :: nxm=64, nym=64, nxfft = 2*nxm, nyfft=2*nym

end module mod_fft_param

