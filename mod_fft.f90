! Created by me on 01/06/2021.

module mod_fft
  use mod_params
  use mod_utils
  use FFTW3

  implicit none

contains

  subroutine fftshift(ztemp, h)
    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: ztemp
    complex ( C_DOUBLE_COMPLEX ), dimension(:,:), allocatable :: h

    integer :: ix, iy
    integer :: nxfft, nyfft

    nxfft = size(ztemp,1)
    nyfft = size(ztemp,2)
    !  on periodise a nouveau pour remettre dans l'ordre
    !
    do ix=1,nxfft/2
       do iy=1,nyfft/2
          h(ix,iy)= ztemp(nxfft/2+ix,nyfft/2+iy)

          h(nxfft-ix+1,nyfft-iy+1)=ztemp(nxfft/2-ix+1,nyfft/2-iy+1)
          h(ix,nyfft-iy+1)=ztemp(nxfft/2+ix,nyfft/2-iy+1)
          h(nxfft-ix+1,iy)=ztemp(nxfft/2-ix+1,nyfft/2+iy)
       enddo
    enddo

  end subroutine fftshift


  subroutine FFT2Dpsf( g, y )
    ! entree :
    ! g signal, taille implicite
    double precision, dimension(:, :), intent(in) :: g
    complex ( C_DOUBLE_COMPLEX ), dimension(:, :), intent(inout) :: y

    integer :: nxfft, nyfft

    type ( C_PTR )       :: plan2f, plan2b
    integer          :: iret, omp_get_max_threads ! pour FFT en parallelisation

    complex ( C_DOUBLE_COMPLEX ), dimension(:, :), allocatable :: g2
    complex ( C_DOUBLE_COMPLEX ), dimension(:, :), allocatable :: zg2
    integer :: ix, iy

    integer :: Nx, Ny
    integer :: nxg2, nyg2


    nxfft = size(y, 1)
    nyfft = size(y, 2)

    call dfftw_init_threads(iret)
    call dfftw_plan_with_nthreads(omp_get_max_threads())

#ifdef ALGO_KAMAL
    nxg2 = 2*nxfft
    nyg2 = 2*nyfft
#else
    print*, __FILE__, __LINE__, ' FIXME on calcule la FFT sans zero  &
         & padding ni periodisation ', nxfft, nyfft
    nxg2 = nxfft
    nyg2 = nyfft
#endif
    allocate( g2(nxg2, nyg2) )  ! pour stocker g doublée par le zero padding
    allocate( zg2(nxg2, nyg2) ) ! pour stocker la TF de g2

    call dfftw_plan_dft_2d(plan2f, nxfft, nyfft, g2, zg2,&
         & FFTW_FORWARD ,FFTW_ESTIMATE)

#ifdef DEBUG2
    print*, __FILE__, __LINE__, ' calcul FFT 2d de la PSF'
#endif
    ! calcul la fft du signal g de dimension Nx, Ny
    ! on peut choisir le même nombre de points pour la FFT: Nxfft = Nx ou prendre plus grand
    ! renvoie un tableau de dimension Nxfft, Nyfft

    nx = size(g, 1)
    ny = size(g, 2)
#ifdef ALGO_KAMAL
    ! zero-padding
    g2 = 0.0d0
    do ix = 1,Nx
       do iy = 1,Ny
          g2(((nxg2-Nx)/2) + ix, ((nyg2-Ny)/2) + iy) = g(ix, iy)
       end do
    end do
    ! periodisation
    g2 = cshift(g2, nxg2/2, 1)
    g2 = cshift(g2, nyg2/2, 2)
#else
    g2 = g
#endif

    call dfftw_execute_dft(plan2f, g2, zg2)
    open(unit =12, file = 'zg2_FFT_avant_cshift.txt')
    !write(12,*) "# zg2 FFT avant cshift ", nxg2, nyg2
    do ix=1,nxg2
       do iy=1,nyg2
          write(12,'( 2I5 , 2(1 X , E25.16) )') ix, iy, &
               & real(zg2 ( ix, iy )) ! , imag(zg2 ( ix, iy ))
       enddo
    enddo
    close(12)

#ifdef ALGO_KAMAL
    zg2 = cshift(real(zg2), nxg2/2, 1)
    zg2 = cshift(real(zg2), nyg2/2, 2)
    open(unit =12, file = 'zg2_FFT_apres_cshift.txt')
    write(12,*) "# zg2 FFT apres cshift", nxg2, nyg2
    do ix=1,nxg2
       do iy=1,nyg2
          write(12,'( 2I5 , 2(1 X , E25.16) )') ix, iy, &
               & real(zg2 ( ix, iy )), imag(zg2 ( ix, iy ))
       enddo
    enddo
    close(12)

    y(:, :) = zg2(nxfft/2 + 1:nxfft/2 + nxfft,nyfft/2 + 1:nyfft/2 + nyfft)
    open(unit =12, file = 'y_fft.txt')
    write(12,*) "# y FFT de taille nxfft*nyfft", nxfft, nyfft
    do ix=1,nxfft
       do iy=1,nyfft
          write(12,'( 2I5 , 2(1 X , E25.16) )') ix, iy, &
               & real(y ( ix, iy )), imag(y ( ix, iy ))
       enddo
    enddo
    close(12)
#else
    y = zg2
#endif

    deallocate(g2)
    deallocate(zg2)
  end subroutine FFT2Dpsf

end module mod_fft
