!================================================================
! contient divers constantes et routines mathematiques
!
! Kamal BELKEBIR (novembre 2020)
!================================================================
module mod_maths
  implicit none

  ! cstes mathematiques
  double precision, parameter :: pi = dacos( -1.0D0 )
  double precision, parameter :: twopi = 2.0D0*pi

  double complex   :: ucomp
  double complex   :: icomp
  double complex   :: zcomp

  parameter ( ucomp = dcmplx(1.0D0, 0.0D0) )
  parameter ( icomp = dcmplx(0.0D0, 1.0D0) )
  parameter ( zcomp = dcmplx(0.0D0, 0.0D0) )

contains
  !================================================================
  ! Moyenne d'un tableau de complexes
  subroutine moyenne_c( f, n, moy )
    implicit none
    double complex, dimension(:), intent(in) :: f
    integer, intent(in)                   :: n
    double complex,intent(out)            :: moy
    integer                               :: i
    double complex                        :: ztemp

    ztemp = dcmplx( 0.0D0,0.0D0 )
    do i = 1, n
       ztemp = ztemp+f( i )
    enddo
    moy = ztemp/dble( n )
  end subroutine moyenne_c

  !================================================================
  ! Moyenne d'un tableau de reels
  subroutine moyenne_r( f, n, moy )
    implicit none
    double precision, dimension(:), intent(in) :: f
    integer, intent(in)                     :: n
    double precision, intent(out)           :: moy
    integer                                 :: i
    double precision                        :: ztemp

    ztemp = dcmplx( 0.0D0,0.0D0 )
    do i = 1, n
       ztemp = ztemp +f(i)
    enddo
    moy = ztemp/dble(n)
  end subroutine moyenne_r
  !================================================================

  !================================================================
  ! calcule une gaussienne de centre (centrex,centrey) et d'ecart type
  ! sigma.
  !  subroutine gaussienne2D( x, y, gaussienne, sigma, centrex, centrey )
  !    implicit none
  !    double precision, dimension(:), intent(in) :: x, y
  !    double precision, intent(in) :: centrex, centrey
  !    double precision, intent(in) :: sigma
  !
  !    double precision, dimension(:,:), intent(out) :: gaussienne
  !
  !    ! variables locales
  !    integer          :: ix, iy, nx, ny
  !    double precision :: a, r, rx, ry
  !
  !    nx = size(x)
  !    ny = size(y)
  !    a = sigma*sigma * 2.0d0 * pi
  !    do ix = 1,nx
  !       do iy = 1,ny
  !          rx                    = x( ix ) - centrex
  !          ry                    = y( iy ) - centrey
  !          r                     = rx*rx + ry*ry
  !          r                     = r / (sigma*sigma)
  !          gaussienne ( ix, iy ) = exp(-r)/a
  !       enddo
  !    enddo
  !  end subroutine gaussienne2D

  !================================================================
  ! comparaison des tableaux f et g supposes de même taille

  subroutine  rms_2D( f, g, normef, normeg, rms )
    implicit none
    double precision, dimension(:,:), intent(in) :: f, g
    double precision, intent(out) :: rms, normef, normeg

    integer :: ix, iy
    integer :: nx, ny

#ifdef DEBUG
    print*, __FILE__, __LINE__,' rms_2D '
#endif

    nx =  size(f,1)
    ny =  size(f,2)

    rms    = 0.0D0
    normef = 0.0D0
    normeg = 0.0D0
    do ix = 1,nx
       do iy = 1,ny
          normef = normef+abs( f( ix,iy ) )**2
          normeg = normeg+abs( g( ix,iy ) )**2
          rms    = rms + abs( f( ix,iy ) - g( ix,iy ) )**2
       enddo
    enddo
#ifdef DEBUG2
    print*, __FILE__, __LINE__,' rms is ', rms
#endif
    normef = sqrt( normef ) / sqrt (dble(nx*ny))
    normeg = sqrt( normeg ) / sqrt (dble(nx*ny))
    if (normef > 0) then
       rms    = sqrt( rms) / normef
    else
#ifdef DEBUG
       print*, __FILE__, __LINE__,' Error divide by 0 '
#endif
       rms = 999999.
    end if
  end subroutine rms_2D

  ! calcul de la norme L2 de f dans R^3
  ! on s'inspire de la page wikipedia
  ! https://fr.wikipedia.org/wiki/Norme_(math%C3%A9matiques)
  ! en normalisant avec la norme infinie
  subroutine norme_fortran(f,nx,ny,nz,normef)
    integer :: ix, iy, iz, nx, ny, nz
    double precision :: normef, f(nx,ny,nz)

    doubleprecision :: valmax ! norme infinie

    valmax = maxval(abs(f(:,:,:)))
    !valmax = 1.0d0
    normef =0.0D0
    do iz = 1,nz
       do iy = 1,ny
          do ix = 1,nx
             normef = normef+abs(f(ix,iy,iz)/valmax)**2
          enddo
       enddo
    enddo
    normef = valmax*sqrt(normef)
#ifdef DEBUG
    print *, "normef ", normef
    print *, "norm2 fortran ", norm2(real(f(:, :, :)))
    print *, "norm2 fortran au carré ", norm2(real(f(:, :, :)))**2
    print *, "moyenne ", sum(f)/(max(1,size(f)))
#endif

  end subroutine norme_fortran

  subroutine norme_l2(z,nx,ny,norme)
    double precision, dimension(:, :), intent(in) :: z
    integer :: nx, ny
    double precision, intent(out) :: norme

    integer :: ix, iy
    double precision :: valmax ! norme infinie

    valmax = maxval(abs(z(:,:)))
    !valmax = 1.0d0
    norme =0.0D0
    do iy = 1,ny
       do ix = 1,nx
          norme = norme+abs(z(ix,iy)/valmax)**2
       enddo
    enddo
    norme = valmax*sqrt(norme)
#ifdef DEBUG2
    print *, "norme ", norme
    print *, "norm2 fortran ", norm2(real(z(:, :)))
    print *, "norm2 fortran au carré ", norm2(real(z(:, :)))**2
    print *, "moyenne ", sum(z)/(max(1,size(z)))
#endif

  end subroutine norme_l2

end module mod_maths
