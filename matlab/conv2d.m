%% Tests de différents produits de convolution en 2D
clear all
close all
tiledlayout(3,2);

% deux portes 2D
A = zeros(128);
A(62:66,62:66) = ones(5);
nexttile; 
mesh(A)
title('Porte 2D');

Csame = conv2(A,A, 'same')
nexttile; 
surf(Csame)
title('prod de conv. same ');

Cfull = conv2(A,A)
nexttile; 
surf(Cfull)
title('prod de conv. full');

nexttile; 
surf(Cfull)
xlim([64 192])
ylim([64 192])
title('prod de conv. full 96 224');

% attention ici ce n'est pas valide car le résultat pour deux tableaux identiques
% est une seule valeur
% il faut donc prendre A et B avec B sous forme de Dirac par exemple 
% Cvalid = conv2(A,A, 'valid')
% title('prod de conv. valid');
% subplot(2,2,4); 
%%surf(Cvalid)

% calcul de la convolution à partir du produits des fft
fft_A = fft2(A);
nexttile; 
surf(real(fft_A))
title('fft A');

Cfull_bis = real(ifftshift(ifft2( fft_A .*fft_A )));
nexttile; 
surf(Cfull_bis)
title('prod de conv. full par les fft');


%% ce script calcul le produit de convolution de 2 gaussiennes 
sigma = 1;
center(1) = 0;
center(2) = 0;
mat = zeros(64, 64);
g = gauss2d(mat, sigma, center);
subplot(2,2,1); 
title('Gaussienne');
surf(g)

Cfull = conv2(g,g)
subplot(2,2,2); 
title('prod de conv.');
surf(Cfull)




function mat = gauss2d(mat, sigma, center)
gsize = size(mat);
[R,C] = ndgrid(1:gsize(1), 1:gsize(2));
mat = gaussC(R,C, sigma, center);
end

function val = gaussC(x, y, sigma, center)
xc = center(1);
yc = center(2);
a = 0.5 / (sigma.^2)
exponent = ((x-xc).^2 + (y-yc).^2)*a;
val       = (exp(-exponent))/(sigma*sigma*2*pi);
end

%%
