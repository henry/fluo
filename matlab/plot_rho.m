for i=0:3
    fichier=strcat('../runs/nouveau/inverse/rhon_apres_000', num2str(i), '_z1.txt');
    Z=importdata(fichier);
    plot3(Z(1:1:end,1), Z(1:1:end,2), Z(1:1:end,3),'.')
    title(strcat('rho: iter ', num2str(i)))
    zlim([-2 1])
    view(90,0)

    pause(2);
end

