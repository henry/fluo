import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

DataAll1D = np.loadtxt("/home/me/AMUbox/projets/fluo/runs/code_kamal_orig/gauss_f_2d2.mat")

# create 2d x,y grid (both X and Y will be 2d)
X, Y = np.meshgrid(DataAll1D[:,0], DataAll1D[:,1])

# repeat Z to make it a 2d grid
Z = np.tile(DataAll1D[:,2], (len(DataAll1D[:,2]), 1))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot_surface(X, Y, Z, cmap='ocean')
#ax.plot_surface(X, Y, Z)
#ax.plot_wireframe(X, Y, Z, color='black')

#plt.colorbar()
plt.show()

import csv

with open('/home/me/AMUbox/projets/fluo/runs/code_kamal_orig/real_conv2d_analy.mat', newline='') as csvfile:
    data = list(csv.reader(csvfile))

print(data)
f = [d[0].split()[2] for d in data]
fn = np.array(f, dtype=np.float64)

with open('/home/me/AMUbox/projets/fluo/runs/code_kamal_orig/real_conv2d.mat', newline='') as csvfile:
    data = list(csv.reader(csvfile))

print(data)
g = [d[0].split()[2] for d in data]
gn = np.array(g, dtype=np.float64)

l2_norm = np.linalg.norm(fn-gn)
