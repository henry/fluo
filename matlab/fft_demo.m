
clear all
close all

%% exemple 0 : fonction porte
N = 128
Nfft = 128
fs = 0.5;
fs = 12/1000
fs = 0.01
fs = 12/1023
fs = 12/127
lx = (N-1)*fs
t = linspace(-lx/2,lx/2, N+1);  % lx = (N-1) dx

f = 0*t;
A = (N/4)*fs; % largeur de l ouverture de la porte
f((N/2+1)-N/8:(N/2+1)+N/8) = 1;
plot(t,f,'-o')
xlim([-lx/2 lx/2])
%ylim([-0.1, 1.5])
title('signal porte')

%% calcul de la TF analytique :  un sinus cardinal
nu = -lx/2:fs:lx/2;
Fanalytic = A*(sin(pi*A*nu))./(pi*A*nu);
figure
plot(nu,Fanalytic,'LineWidth',2)
title('Fanalytic')

%%

% calcul avec une integration numerique classique (trapeze)
Ftrapz = zeros(size(nu));
for nu_idx = 1:length(nu)
    for t_idx = 1:length(t)
        ndelta_t = t(t_idx);
        Ftrapz(nu_idx) = Ftrapz(nu_idx) +  f(t_idx)*exp(-2*1i*pi*nu(nu_idx)*ndelta_t)*fs;
    end
end
hold on
plot(nu, real(Ftrapz),'--','LineWidth',2)

%%
% calcul par fft 
% attention dans la fft on considere que le
% le premier element du vecteur est l'element zero, suivi des coordonnees
% positives suivi des coordonnees negatives
% il convient donc d'utiliser fftshift pour placer la seconde moitie du
% vecteur au debut (de n/2 + 1 à la fin) suivi de la premiere moitie du
% vecteur (de 1 à n/2).
% comme on considere cette fois le 0, on doit placer une valeur de moins à 1
% dans les positif (pour etre symetrique par rapport au 0 pour respecter la
% parité de la fonction)Fanalytic

f2 = f(1:end-1);
%f2(641) = 1;
%f2(385) = 0;
Ffft = fft(fftshift(f2));
a = fftshift(real(Ffft)); 
n = length(f)-1; 
freqAxis = (-n/2:n/2-1)*((1/fs)/n); % les frequences de calcul vont de -n/2 a +n/2-1 fois la frequence de l'echantillon fois sa longueur


plot(freqAxis,a*fs,'+')
xlim([-lx/2,lx/2])
title('trois manieres de calculer la TF')
legend('analytical solution','trapz', 'fft')

fileID = fopen('exp.txt','w');
for idx = 1:length(Fanalytic)
%fprintf(fileID,'%f %f %f %f\n', nu, freqAxis, Fanalytic, a*fs);
fprintf(fileID,'%12.8f %12.8f %12.8f %12.8f\n', nu(idx), freqAxis(idx), Fanalytic(idx), a(idx)*fs);
end
fclose(fileID);




















