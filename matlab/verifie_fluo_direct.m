%% Attention les fichiers de données doivent contenir une première ligne de commentaire qui légende les colonnes
% sinon il faut utiliser laod() au lieu de import()

close all
clearvars

tiledlayout(3,1);

%% verification des images produites par le code fluo_direct

chemin="/home/me/projets/fluo/runs/tests/gaussiennes";
chemin="/home/me/projets/fluo/runs/tests/deuxgaussiennesidentiques/";
chemin="/home/me/projets/fluo/runs/nouveau/direct";
chemin="/home/me/projets/fluo/runs/tests/portes";
chemin="/home/me/projets/fluo/runs/tests/demo";

%%
% on commence par verifier le signal de densite
fichier=fullfile(chemin, "rho2d_0001.txt");
temp=importdata(fichier);
image=temp.data;
nexttile; 
Z = image(:,3);
Z = reshape(Z,128,128);
surf(Z)
hold on
imagesc(Z)
colorbar
title("Densité")

%%
fichier=fullfile(chemin, "psf2d_0001.txt");
temp=importdata(fichier);
image=temp.data;
nexttile; 
Z = image(:,3);
Z = reshape(Z,128,128);
surf(Z)
hold on
imagesc(Z)
colorbar
title("PSF 2D")

%%
fichier=fullfile(chemin, "data_0001.txt");
image=load(fichier,'-ascii');
nexttile; 
Z = image(:,3);
Z = reshape(Z,128,128);
surf(Z)
hold on
imagesc(Z)
colorbar
title("résultat ")

pause(10);
