%% On affiche les courbes produites par le code fluo trouvé ici
% https://gitlab.fresnel.fr/henry/fluo
% le fichier mesures_direct est produit par le code fluo_direct
% dans le repertoire runs/nouveau/direct, lancer
% fluo_direct fluo_direct.IN
% qui produit le fichier des mesures
% recopier ce fichier de mesures dans le dossier runs/nouveau/inverse
% les autres fichiers dont produits par le code fluo_inverse

% mettre le même nom de fichier que celui déclaré dans fluo_inverse.IN
fichier='../runs/nouveau/direct/mesures_direct.txt';
fichier='/home/me/AMUbox/projets/fluo/runs/nouveau/direct/data_0001.txt';
% t=load(fichier, '-ascii');
% 
% surf(t(0),t(1),t(2));

M=importdata(fichier);
%% Z.data;
%Z=Z.data;

%%
plot3(M(1:1:end,1), M(1:1:end,2), M(1:1:end,3),'.')
%plot3(Z(1:10:end,1), Z(1:10:end,2), Z(1:10:end,3),'+')
% [xx, yy, zz] = meshgrid(Z(:,1), Z(:,2), Z(:,3));
% surf(xx,yy,zz)
title('Mesures en entrée')
% xlabel('t (milliseconds)')
% ylabel('X(t)')
grid on
grid minor
xlabel('ix')
ylabel('iy')
zlabel('mes')
%view(90,0)


%% aide: https://fr.mathworks.com/help/matlab/ref/plot3.html
%

for i=1:3
    tiledlayout(3,3)
    
    ax0 = nexttile;
    s1 = plot3(M(1:1:end,1), M(1:1:end,2), M(1:1:end,3),'.')
    pbaspect([2 2 1])
    title(ax0, strcat('Mesures'))
    zlim([0 100])
    view(90,0)
    
    % Right plot
    ax2 = nexttile;
    fichier=strcat('../runs/nouveau/inverse/data_000', num2str(i), '.txt');
    Z=importdata(fichier);
    plot3(Z(1:1:end,1), Z(1:1:end,2), Z(1:1:end,3),'.')
    pbaspect([2 2 1])
    title(ax2, strcat('rho I * p iter ', num2str(i)))
    zlim([-100 0])
    view(90,0)

    % Left plot
    ax1 = nexttile;
    % on va plotter le residu a chaque iteration
    fichier=strcat('../runs/nouveau/inverse/residu_000', num2str(i), '.txt');
    Z=importdata(fichier);
    plot3(Z(1:1:end,1), Z(1:1:end,2), Z(1:1:end,3),'.')
    pbaspect([2 2 1])
    title(ax1, strcat('Résidu iter ', num2str(i)))
    zlim([0 100])
    view(90,0)
    
    % deuxieme ligne de graphes
    nexttile;
    % pas de graphique
    nexttile;
     fichier=strcat('../runs/nouveau/inverse/rhon_apres_000', num2str(i), '_z1.txt');
    Z=importdata(fichier);
    plot3(Z(1:1:end,1), Z(1:1:end,2), Z(1:1:end,3),'.')
    title(strcat('rho: iter ', num2str(i)))
    zlim([-2 1])
    view(90,0)

     nexttile;
    % pas de graphique
  
    % troisieme ligne de graphes
    nexttile;
    % pas de graphique
    nexttile;
    fichier=strcat('../runs/nouveau/inverse/gradient_000', num2str(i), '_0001_0001.txt');
    Z=importdata(fichier);
    plot3(Z(1:1:end,1), Z(1:1:end,2), Z(1:1:end,3),'.')
    title(strcat('gradient: iter ', num2str(i)))
    zlim([-10000 1])
    view(90,0)

     nexttile;
    % pas de graphique
  
    
    pause(3);
end
