% plot gaussienne
% J. Wojak 16 juin 21

Nx = 64;
x = (1:Nx)/10;
x0 = 3.2;

Ny = 64;
y = (1:Ny)/10;
y0 = 3.2;

sigma = 1.0;
a = (sigma.^2) *(2*pi);

l = length(x);
f = zeros(l,l);
for ix = 1:l
    for iy = 1:l

    r = ((x(ix)-x0).^2 + (y(iy)-y0).^2)/(sigma.^2);
    f(ix,iy)  = exp(-r)*1/a;

    end
end

% surf(f) % pour visulaiser la gaussienne

% pour visualiser les données produites par le code de Kamal
% Z=xyz2grid("gauss_f_2d2.mat") pour convertir les données lues
% surf(Z)  % et pouvoir les afficher