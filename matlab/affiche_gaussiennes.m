addpath '/home/me/AMUbox/projets/fluo/matlab';

%% une gaussienne 1D
% cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
% load('gauss_1d.mat', '-ASCII')
% plot(gauss_1d(:,1), gauss_1d(:,2),'DisplayName','gauss_1d')
% title('Gaussienne 1D')
% xlabel('x')
%ylabel('cos(5x)')

%%
tiledlayout(2,1)

ax1 = nexttile;
Z = load('gauss_f_2d.mat', '-ASCII')
[xx, yy, zz] = meshgrid(Z(:,1), Z(:,2), Z(:,3))

surf(xx,yy, zz)
%legend('location', 'northwest');
title('gauss\_f\_2d (Kamal)');

%%
cd ~/AMUbox/projets/fluo/runs/nouveau/
Z=xyz2grid("gauss_f_2d.mat");
subplot(9,2,2); surf(Z)
title('gauss\_f\_2d (nouveau)');

% on double le support et on fait du zéro padding
cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
Z=xyz2grid("gauss_g_2d2.mat");
subplot(9,2,3); surf(Z)
title('gauss\_g\_2d2 (Kamal)');

cd ~/AMUbox/projets/fluo/runs/nouveau/
Z=xyz2grid("gauss_g_2d2.mat");
subplot(9,2,4); surf(Z)
title('gauss\_g\_2d2 (nouveau)');

% on trace la TF inverse après shift
cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
subplot(9,2,5); Z=xyz2grid("real_conv2d.mat");
surf(Z)
title('real\_conv2d (Kamal)');

cd ~/AMUbox/projets/fluo/runs/nouveau/
subplot(9,2,6); Z=xyz2grid("real_conv2d.mat");
surf(Z)
title('real\_conv2d (nouveau)');

%
% on trace la FFT
cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
Z=xyz2grid("real_fft_f_2d2.mat");
subplot(9,2,7); surf(Z)
title('real\_fft\_f\_2d2 (Kamal)');

cd ~/AMUbox/projets/fluo/runs/nouveau/
Z=xyz2grid("real_fft_g_2d2.mat");
subplot(9,2,8); surf(Z)
title('real\_fft\_g\_2d2 (nouveau)');
%hold on

cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
Z=xyz2grid("imag_fft_f_2d2.mat");
subplot(9,2,9); surf(Z)
title('imag\_fft\_f\_2d2 (Kamal)');

cd ~/AMUbox/projets/fluo/runs/nouveau/
Z=xyz2grid("imag_fft_g_2d2.mat");
subplot(9,2,10); surf(Z)
title('imag\_fft\_g\_2d2 (nouveau)');
%

% %% on trace la TF inverse du produit de convolution
% cd ~/AMUbox/projets/fluo/runs/nouveau/
% Z=xyz2grid("raw_real_h2.mat")
% surf(Z)
% %%
% %% on trace la TF inverse du produit de convolution
% Z=xyz2grid("raw_imag_h2.mat")
% surf(Z)

% au final le produit de convolution
cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
Z=xyz2grid("real_conv2d.mat");
subplot(9,2,11); surf(Z)
title('prod. de conv. réel (Kamal)');
% subplot(9,2,13); surf(Z)
% title('prod. de conv. imag (Kamal)');
% Z=xyz2grid("imag_conv2d.mat")
% surf(Z)


% on regarde le resultat h realh_conv2d.mat
cd ~/AMUbox/projets/fluo/runs/nouveau/
Z=xyz2grid("real_conv2d.mat");
subplot(9,2,12); surf(Z)
title('prod. de conv. réel (nouveau)');

%%
cd ~/AMUbox/projets/fluo/runs/code_kamal_orig/
Z=xyz2grid("imag_fft_f_2d2.mat")
subplot(9,2,9); surf(Z)
title('imag\_fft\_f\_2d2 (Kamal)');

cd ~/AMUbox/projets/fluo/runs/nouveau/
Z=xyz2grid("imag_fft_g_2d2.mat")
subplot(9,2,10); surf(Z)
title('imag\_fft\_g\_2d2 (nouveau)');

sgtitle('Subplot Grid Title') 