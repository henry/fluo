% plot gaussienne
% J. Wojak 16 juin 21


Nx = 64;
Nx = 128;
x = (1:Nx)/10;
fs = 12/(Nx-1)
lx = (Nx-1)*fs
x = linspace(-lx/2,lx/2, Nx+1); 
x0 = 0.0; % 1.2;

Ny = 64;
Ny = 128;
y = (1:Ny)/10;
y0 = 0.0; %1.2;
fs = 12/(Ny-1)
ly = (Ny-1)*fs
y = linspace(-lx/2,lx/2, Ny+1); 

sigma = 0.2;
a = (sigma.^2) *(2*pi);

l = length(x);
f = zeros(l,l);
for ix = 1:l
    for iy = 1:l

    r = ((x(ix)-x0).^2 + (y(iy)-y0).^2)/(sigma.^2);
    f(ix,iy)  = exp(-r)*1/a;

    end
end

% surf(f) % pour visulaiser la gaussienne
surf(x,y,f)
title(strcat('Gaussienne sigma = ', num2str(sigma)))
% xlabel('t (milliseconds)')
% ylabel('X(t)')
grid on
grid minor
% pour visualiser les données produites par le code de Kamal
% Z=xyz2grid("gauss_f_2d2.mat") pour convertir les données lues
% surf(Z)  % et pouvoir les afficher