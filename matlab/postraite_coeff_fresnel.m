%% tracé des résultats du code de calcul des coefficients de Fresnel
% voir 
%tiledlayout(3,1);

chemin="/home/me/projets/fluo/cmake-build-debug/";
chemin="/home/me/projets/tmp/fluo/build/";
chemin="/home/me/projets/fluo/runs/tests/coeff_fresnel/";

% on commence par r//
fichier=fullfile(chemin, "refl_para.dat");
r=load(fichier,'-ascii');
%%
%nexttile; 
figure
plot(r(:,1), r(:,2))
hold on
grid on
grid minor
xlabel("degres")
ylabel("r// au carré")
title("réflexion")
xlim([0 90])

fichier=fullfile(chemin, "tran_para.dat");
t=load(fichier,'-ascii');
%nexttile; 
plot(t(:,1), t(:,2))

