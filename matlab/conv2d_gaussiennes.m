%% Tests de différents produits de convolution en 2D
clearvars
close all
tiledlayout(3,2);

Nxfft = 1024;
Nyfft = 1024;

%% gaussienne idem à celle du code fortran
Nx = 128;
fs = 12/(Nx-1);
lx = (Nx-1)*fs;
x = linspace(-lx/2,lx/2, Nx+1); 
x0 = 0.0; % 1.2;
dx = 12/Nx; %0.0938;

Ny = 128;
y0 = 0.0; %1.2;
fs = 12/(Ny-1);
ly = (Ny-1)*fs;
y = linspace(-lx/2,lx/2, Ny+1); 
dy = 12/Ny; %0.0938;

sigma = 1.0;
a = (sigma.^2)*(2*pi);

l = length(x);
f = zeros(l,l);
for ix = 1:l
    for iy = 1:l

    r = ((x(ix)-x0).^2 + (y(iy)-y0).^2)/(2*sigma.^2);
    f(ix,iy)  = exp(-r)*1/a;

    end
end

%%
%surf(ix,iy,f);
%surf(f);
%%

A=f;
nexttile; 
mesh(A)
title('une gaussienne');

%%
Csame = conv2(A,A, 'same')*dx*dy;
nexttile; 
surf(Csame)
title('prod de conv. same ');

%%
Cfull = conv2(A,A)*dx*dy;
nexttile; 
surf(Cfull)
title('prod de conv. full');


% calcul de la convolution à partir du produits des fft
A_zeros_pad = zeros(Nxfft:Nyfft);
A_zeros_pad((Nxfft-Nx)/2:(Nxfft+Nx)/2,(Nxfft-Nx)/2:(Nxfft+Nx)/2) = A;
fft_A_zeros_pad = fft2(A_zeros_pad);
nexttile; 
surf(abs(fft_A_zeros_pad))
title('fft A');

Cfull_bis = abs(ifftshift(ifft2( fft_A_zeros_pad .*fft_A_zeros_pad )));
nexttile; 
surf(Cfull_bis((Nxfft-Nx)/2:(Nxfft+Nx)/2,(Nxfft-Nx)/2:(Nxfft+Nx)/2)*dx*dy)
title('prod de conv. full par les fft');

% on affiche le max du produit pour verifier avec le fortran
max_conv = max(Csame,[],'all') 

%% solution analytique
% pechée ici: http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/04/maths/pdf/convol8.pdf
fconvf = zeros(l,l);
a = (sigma^2) *(4*pi);
max_analy = 0;
for ix = 1:l
    for iy = 1:l
    r = ((x(ix)-(2*x0)).^2 + (y(iy)-(2*y0)).^2)/(4*sigma^2);
    fconvf(ix,iy)  = exp(-r)*1/a;
    if fconvf(ix,iy) > max_analy
        max_analy = fconvf(ix,iy);
    end
    end
end
nexttile; 
mesh(fconvf)
title('Conv analytique 2D de deux gaussiennes, sigma=1');
max_analy