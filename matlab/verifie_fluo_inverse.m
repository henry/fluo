close all
clear all

tiledlayout(3,1);

%% verification des images produites par le code fluo_inverse

chemin="/home/me/projets/fluo/runs/nouveau/inverse";
chemin="/home/me/projets/fluo/runs/tests/demo";
chemin="/home/me/projets/fluo/runs/tests/portes";
chemin="/home/me/projets/fluo/runs/tests/gaussiennes"
chemin="/home/me/projets/fluo/runs/tests/deuxgaussiennesidentiques";

%%
% l'objet initial
%fichier=fullfile(chemin, "deux_gaussiennes_billes.txt");
fichier=fullfile(chemin, "rho2d_0001.txt");
temp=importdata(fichier);
image=temp.data;
nexttile; 
Z = image(:,3);
Z = reshape(Z,128,128);
surf(Z)
hold on
imagesc(Z)
colorbar
title("objet initial ")

%%
% les mesures, ici elles ont été calculées par fluo_direct
fichier=fullfile(chemin, "data_0001.txt");
image=load(fichier,'-ascii');
nexttile; 
Z = image(:,3);
Z = reshape(Z,128,128);
surf(Z)
hold on
imagesc(Z)
colorbar
title("mesures calculées ")

%% le résultat de l'inversion
fichier=fullfile(chemin, "rhon_trouve.txt");
image=load(fichier,'-ascii');
nexttile; 
Z = image(:,4);
Z = reshape(Z,128,128);
surf(Z)
hold on
imagesc(Z)
colorbar
title("résultat inversion")

%% la fonction de cout

fichier=fullfile(chemin, "fort.2220");
fcout=load(fichier);
figure; 
%plot(fcout(:,3),20*log10(fcout(:,4)),'k')
%plot(fcout(:,3),fcout(:,4),'k')
%loglog(fcout(:,3),fcout(:,4))
semilogy(fcout(:,4))

title("fonction de coût")
grid on
grid minor
xlabel("iterations")
ylabel("fcout")
%xlim([1 1000])
ylim([10^-3 1])

pause(10)
