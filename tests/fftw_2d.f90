! Created by me on 20/09/2021.
! http://lyoncalcul.univ-lyon1.fr/ed/DOCS_2019/dft.pdf
! à partir du programme en 1D, on passe au 2D
! et on utilise des fonctions du projet fluo pour initialiser maille_xy() et set_psf()


program fftw_2d
  use mod_params  !! pour lire dans un fichier les paramètres de la simu
  ! FFTW
  use FFTW3
  use mod_signaux
  use mod_fft
  use mod_maths

  implicit none

  double precision, dimension(:), allocatable :: x_grid, y_grid
  ! define Pi
  integer :: i, ix, iy , N, nx, ny
  complex ( C_DOUBLE_COMPLEX ) , dimension (:, :) , allocatable :: ain
  complex ( C_DOUBLE_COMPLEX ) , dimension (:, :) , allocatable :: aout , aini
  real ( dp ) , dimension (:) , allocatable :: x , kx, ky
  real ( dp ) :: dkx, dky
  type ( C_PTR ) :: plan , plani
  real ( C_DOUBLE ) :: fact
  
  character :: ans
  
  ! verification
      double precision :: rms, normef, normeg


  call lecture_params(  )
  ! input data
  N = params_direct%Nx ! 8
  params_direct%Nx = N
  params_direct%Ny = N
  ! allocate
  allocate ( x (N) )
  allocate ( kx (N) )
  allocate ( ky (N) )
  allocate ( ain (N, N) )
  allocate ( aini (N, N) )
  allocate ( aout (N, N) )
  allocate( params_direct%psf( N, N, 1 ) )

  ! make plan forward and backward
  ain = ucomp
  aout = ucomp
  aini = ucomp

  ! normalization
  fact = 1.0_dp / (N*N)
  ! case
  nx      = params_direct%Nx
  ny      = params_direct%Ny
  allocate( x_grid( nx ) )
  allocate( y_grid( ny ) )
  ! calcul les points de discretisation dans l'espace direct
  call maille_xy( x_grid, y_grid )
  call set_signal( params_direct%algopsf, params_direct%psf, x_grid, y_grid, "psf2d" )
!  write (* ,*) " Function in physical space and frequencies "
  do ix=1,N
    do iy=1,N
      write(10,'( 2I5 , 3(1 X , E25.16) )') ix, iy, params_direct%psf(ix, iy, 1)
    enddo
  enddo

  ! execute forward dft
      plan = fftw_plan_dft_2d(N, N, ain, aout, FFTW_FORWARD ,FFTW_ESTIMATE)
    ain(:, :) = params_direct%psf(:, :, 1)
    call fftw_execute_dft ( plan , ain , aout )
  do ix=1,N
     do iy=1,N
       write(15,'( 2I5 , 3(1 X , E25.16) )') ix, iy, real(aout ( ix, iy ))
     enddo
  enddo
  
  write (* ,*) " Fourier coefficient after forward FFT "
  ! TODO FIXME par sur que ce soit bon por representer la fft!?
  ! discrete modes
  dkx = 2.0_dp * PI / params_direct%Lx
  dky = 2.0_dp * PI / params_direct%Ly
  ! hermitian symmetry
  do i =0 , N /2
    kx ( i+1 ) = i * dkx
    ky ( i+1 ) = i * dky
  end do
  do i = N /2+1 , N -1
    kx ( i ) = (N - i ) * dkx
    ky ( i ) = (N - i ) * dky
  end do
  do ix=1,N
     do iy=1,N
       write(11,'( 2I5 , 3(1 X , E25.16) )') ix, iy, &
             & real(aout ( ix, iy ))
     enddo
  enddo

  ! inverse transform
  plani = fftw_plan_dft_2d(N, N, aout, aini, FFTW_BACKWARD, FFTW_ESTIMATE)
  call fftw_execute_dft ( plani , aout , aini )
  write (* ,*) " Function after forward - backward FFT ", params_fft%Nxfft, params_fft%Nyfft
  do ix=1,params_fft%Nxfft
     do iy=1,params_fft%Nyfft
        write(12,'( 2I5 , 4(1 X , E25.16) )') ix, iy, x_grid(ix), y_grid(iy), &
             & params_direct%psf(ix, iy, 1) , real(aini ( ix, iy ))  * fact
     enddo
  enddo

  call rms_2D(params_direct%psf(:, :, 1), real(aini ( :, : ))* fact, normef, normeg, rms)
  print *, "rms: ", rms
  print *, "norme psf avant: ", normef
  print *, "norme psf apres: ", normeg
  
  ! destroy plans
  call fftw_destroy_plan ( plan )
  call fftw_destroy_plan ( plani )
  ! deallocate
  deallocate ( x )
  deallocate ( ain )
  deallocate ( aini )
  deallocate ( aout )

  if (rms > 10e-6) then
    print *, "Erreur trop grande!"
    stop -1
    else
  print *, "pour plotter le resultat, dans gnuplot: "
  print *, "splot 'runs/tests/psf_2D.txt', 'runs/tests/fort.12' using 3:4:6"
  print *, "a comparer avec le script matlab plot_gaussienne.m"
  end if
end program fftw_2d
