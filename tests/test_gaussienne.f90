!%% https://www.gaussianwaves.com/2014/07/how-to-plot-fft-using-matlab-fft-of-basic-signals-sine-and-cosine-waves/
program test_gaussienne
  use, intrinsic :: iso_c_binding
  use mod_utils
  use mod_params  !! pour lire dans un fichier les paramètres de la simu
  implicit none

  double precision, dimension(:,:, :), allocatable   :: tab
  double precision, dimension(:), allocatable :: x_grid, y_grid
  double precision :: x_0, y_0
  
  double precision :: sigma
  character(*), parameter :: fichier='gaussienne_bille_centree.txt'
!  character(*), parameter :: fichier='gaussienne_psf_centree.txt'
!  character(*), parameter :: fichier='gaussienne_bille_decentree.txt'
!  character(*), parameter :: fichier='gaussienne_psf_decentree.txt'
  character(*), parameter :: fichiersomme='deux_gaussiennes_billes.txt'

  integer :: ix, iy, N

  IF( COMMAND_ARGUMENT_COUNT() /= 1 )THEN
    WRITE(*, *)'ERROR, ONE COMMAND-LINE ARGUMENTS REQUIRED, STOPPING'
    WRITE(*, *)'Usage:  mon_prog mes_params.IN'
    STOP 1
  ENDIF
  call lecture_params()
  
  allocate(x_grid(params_direct%Nx))
  allocate(y_grid(params_direct%Ny))
  call maille_xy( x_grid, y_grid )

  allocate( tab( params_direct%Nx, params_direct%Ny, params_direct%Nz ) )

  x_0 = 0.0d0
  y_0 = 0.0d0
  sigma = 1.0d0 ! on prend un petit sigma
!  x_0 = -2.0d0
!  y_0 = -2.0d0
  !sigma = 0.2d0 ! on prend un petit sigma
  call gaussienne2D(x_grid, y_grid, tab, x_0, y_0, sigma)

  open(unit=11, file=fichier )
  !write(11,*) "# x y "
  do ix=1,params_direct%Nx
    do iy=1,params_direct%Ny
      if (tab( ix, iy, 1 ) < 1.0e-8) then
        ! on fait ça à cause de gnuplot qui n'aime pas les chiffres en 10e-173
      write(11, fmt='(2i4,2f19.15,e25.17)') ix, iy, x_grid(ix), y_grid(iy), 0.0d0
        else
        write(11, fmt='(2i4,2f19.15,e25.17)') ix, iy, x_grid(ix), y_grid(iy), 25.0d0*  tab( ix, iy, 1 )
        !write(11, fmt='(2i4,2f19.15,e25.17)') ix, iy, x_grid(ix), y_grid(iy), tab( ix, iy, 1 )
        end if
    enddo
  enddo
  close(11)
  
  print *, "fin creation fichier ", fichier
  
  tab = 0.0d0
  call somme2gaussienne2D(x_grid, y_grid, tab, -2.0d0, -2.0d0, sigma, 2.0d0 ,2.0d0, sigma)
  open(unit=11, file=fichiersomme )
  !write(11,*) "# x y "
  do ix=1,params_direct%Nx
    do iy=1,params_direct%Ny
      write(11,fmt='(2i4,2f19.15,e25.17)') ix, iy, x_grid(ix), y_grid(iy), tab( ix, iy, 1 )
    enddo
  enddo
  close(11)
  print *, "fin creation fichier ", fichiersomme

end program test_gaussienne

