les programmes de tests avec la lib FFTW3

# fftw_1d.f90
pour vérifier les calculs, on peut tracer dans octave la fonction cos-sin avec le script suivant:
```shell
omega=3
N=16384
x=linspace(0,2*pi,N+1);
ain=cos(omega*x) + (0.10*sin(10*omega*x));
plot(x,ain,'LineWidth',2)
fileID = fopen('exp.txt','w');
for idx = 1:length(ain)
fprintf(fileID,'%d %12.8f %12.8f\n', idx, x(idx), ain(idx));
end
fclose(fileID);
```
et on peut comparer les deux fichiers fort.10 et exp.txt.

Pour tracer avec gnuplot:
```shell
set grid
plot "../cmake-build-debug/tests/fort.10" using 1:3 w l lw 2 title "periodic-cos-sin"
```

# test_sine.f90
ici, il s'agit de calculer la FFT d'un sinus tronqué, on peut modifier la taille du sinus (nCyl pour nombre de cycles)
et le déphasage initial (phase)
Un simple tracé avec gnuplot des fichiers **sine** pour le signal à échantillonner
```shell
gnuplot> plot "../cmake-build-debug/tests/sine" using 2:3 w l lw 2 title "sin"
```
et la FFT dans le fichier **affa**

**FIXME** l'amplitude des raies du spectres n'est pas calculée de la même façon dans les deux exemples !?