! Created by me on 20/09/2021.
! http://lyoncalcul.univ-lyon1.fr/ed/DOCS_2019/dft.pdf

module mod_signaux
  implicit none

  ! double precision
  integer, parameter :: dp = selected_real_kind(15,50)

contains
  subroutine function_periodic_cos_sin (N ,x , kx , ain )
    ! cos ( omega x ) +0.1* sin (10* omega * x )
    integer , intent ( in ) :: N
    real ( dp ) , intent ( out ) :: x (0: N -1) , kx (0: N -1)
    complex ( dp ) , intent ( out ) :: ain (0: N -1)
    ! local
    real ( dp ) :: Lx , omega
    real ( dp ) , parameter :: PI = 4.0_dp * ATAN (1.0_dp )
    real ( dp ) :: dx , dkx
    integer :: i
    ! box size
    Lx = 2.0_dp * PI * 1.0_dp
    ! spatial discretizatio n
    dx = Lx / real ( N )
    ! discrete function
    omega = 3.0_dp
    ! exclude periodic point
    do i =0 ,N -1
       x ( i ) = real ( i ) * dx
       ain ( i ) = cos ( omega * x ( i ) ) +0.10* sin (10.* omega * x ( i ) )
    end do
    ! discrete modes
    dkx = 2.0_dp * PI / Lx
    ! hermitian symmetry
    do i =0 , N /2
       kx ( i ) = i * dkx
    end do
    do i = N /2+1 , N -1
       kx ( i ) = (N - i ) * dkx
    end do
  endsubroutine function_periodic_cos_sin
end module mod_signaux
