!%% https://www.gaussianwaves.com/2014/07/how-to-plot-fft-using-matlab-fft-of-basic-signals-sine-and-cosine-waves/
program test_sine
  use, intrinsic :: iso_c_binding
  use FFTW3

  implicit none

  integer :: n, i, nfft
  double precision, allocatable, dimension(:) :: f
  complex ( C_DOUBLE_COMPLEX ), allocatable, dimension(:) ::f2, fft2
  double precision e, dx, lx, lxnfft
  double precision, allocatable, dimension(:) :: x, xfft, FreqAxis
  type ( C_PTR ) :: plan , plani
  complex ( C_DOUBLE_COMPLEX ) ucomp
  
  ! domaine
  double precision :: xmin
  
  ! fonction analytique
  double precision , dimension (:) , allocatable :: f_analy

  double precision , parameter :: PI = 4.0d0 * atan (1.0d0 )
  doubleprecision :: freq, fs, phase
  integer :: overSampRate, nCyl

  ucomp = cmplx(1.0D0,0.0D0)
  ! on se fixe le pas, quel que soit le nbre d'echantillons (parce que la résolution ne dépendant pas de la
  ! frequence d'echanbtillonnage mais seulement de la duree d'observation (lx)
  ! une valeur coherente avec la fonction a echantillonner
  ! ! en partant d'un N faible et qu'on va faire varier en l'augmentant

  freq = 10 ! freq du sinus
  overSampRate=30 ! oversampling rate
  fs=overSampRate*freq; ! sampling frequency
  phase = 0.5d0 * pi  ! 1/3*pi ! desired phase shift in radians
  nCyl = 5 ! to generate five cycles of sine wave

  lx = nCyl*1/freq
  print *, "largeur du signal sinusoidal ", lx
  
  dx = 1.0d0/(fs)  ! fs dans le script matlab
  n  = lx/dx
  print *, "nb de points de la fonction ", n
  nfft   = 32
  nfft = 2048
  nfft = n
  nfft = 1024
  print *, "nb de points de la fft ", nfft


  xmin = 0
  
!  dx = lx / dble( n - 1) ! idem Julien fs
  print *, "pas de discretisation ", dx
  
  lxnfft = dble(nfft)*dx ! on prend le meme pas qu'en x meme si c'est le
  print *, "taille du domaine de la fft: (nfft)*dx   ", lxnfft

  allocate(f(n))
  allocate(x(n))

  print *, __FILE__, __LINE__, 'discretisation en x d une fonction' ! JW appelle ça t[]
  print *, '#        i                 x(i)'
  do i=1,n
    x(i) = dble(i - 1)*dx
    print*, i, x(i)
  enddo

  ! la fonction sinusoidale
  print *
  print *, '# i           x(i)                 f(i)'
  do i=1,n
    f(i) = sin(2*pi*freq*x(i) + phase)
    print*, i, x(i), f(i)
  enddo
  ! sauvegarde dans un fichier
  open(unit=11, file= 'sine')
  write(11,*) '#      i      x(i)    f(i)  '
  do i=1,n
    write(11,*) i, x(i), f(i)
  enddo
  close(11)

  allocate(fft2(nfft))
  allocate(f2(nfft)) ! c'est un tableau utilise pour stocker la porte et qui sert pour le calcul de la fft
  allocate(FreqAxis(nfft))

  plan = fftw_plan_dft_1d (Nfft , f2 , fft2 , FFTW_FORWARD ,&
          & FFTW_ESTIMATE )
  plani = fftw_plan_dft_1d (Nfft , fft2 , f2 , FFTW_BACKWARD ,&
          & FFTW_ESTIMATE )

  f2 = 0.0d0
  do i=1, n
    f2(i) = f(i)
  end do
  print *, __FILE__, __LINE__, 'on calcule la FFT'
  call fftw_execute_dft ( plan , f2 , fft2 )
  ! on remet les frequences dans l'ordre
  fft2 = cshift(real(fft2), nfft/2, 1)
  
  ! calcul de     FreqAxis du psectre
  do i = -nfft/2, (nfft/2)-1
    FreqAxis(i+(nfft/2)+1) = dble(i) * (fs/nfft)
  end do
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!! Enregistrement des resultats !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  print*, __FILE__, __LINE__, 'on ecrit les donnees dans le fichier'
  open(unit=11, file= 'affa')
  write(11,*) '#12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789&
        &0123456789012345678901234567890123456789012345678901234567890'
  write(11,*) '#         i                   FreqAxis(i)                  real(f2(i))                 &
          & abs(fft2(i))             real(fft2(i))           aimag(fft2(i))'
  do i=1,nfft
    write(11,*) i, FreqAxis(i), real(f2(i)), abs(fft2(i)), real(fft2(i))*dx, aimag(fft2(i))
  enddo
  close(11)
  print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
  print *, __FILE__, __LINE__, 'plot "../cmake-build-debug/tests/affa" using 2:4 w l'

  deallocate(f)
  deallocate(x)

  end program test_sine

