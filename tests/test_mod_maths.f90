program test_mod_maths
  use mod_maths

  implicit none

  integer, parameter :: dp = kind(0.d0)                   ! double precision

  integer :: nx, ny
  integer :: i, j
  double precision, dimension(4, 4) :: f, g
  double precision :: rms, normef, normeg
  DATA ((f (i, j), j = 1, 4), i = 1, 4) &
          /1., 2., 3., 4., &
          4., 5., 6., 7., &
          4., 5., 6., 7., &
          4., 5., 6., 7./
!  DATA ((f (i, j), j = 1, 4), i = 1, 4) &
!          /1., -2., 3., 0., &
!          0., 0., 0., 0., &
!          0., 0., 0., 0., &
!          0., 0., 0., 0./
  DATA ((g (i, j), j = 1, 4), i = 1, 4) &
       /1., 2., 3., 4., &
       4., 5., 6., 7., &
       4., 5., 6., 7., &
       4., 5., 6., 7./
  nx = 4
  ny = 4

  call norme_l2(f(:, :),nx,ny, normef)
  print *, "normef ", normef

end program test_mod_maths
