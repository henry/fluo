program test_fichiers
  use, intrinsic :: iso_c_binding
  use mod_utils
  use mod_params  !! pour lire dans un fichier les paramètres de la simu
  implicit none

  double precision, dimension(:,:, :), allocatable   :: tab
  double precision, dimension(:), allocatable :: x_grid, y_grid
  double precision :: x_0, y_0
  
  double precision :: sigma

  integer :: ix, iy, N
  
  integer :: type
  
  character (len = LENGTH) :: nom

  IF( COMMAND_ARGUMENT_COUNT() /= 1 )THEN
    WRITE(*, *)'ERROR, ONE COMMAND-LINE ARGUMENTS REQUIRED, STOPPING'
    WRITE(*, *)'Usage:  mon_prog mes_params.IN'
    STOP 1
  ENDIF
  call lecture_params()
  
  allocate(x_grid(params_direct%Nx))
  allocate(y_grid(params_direct%Ny))
  call maille_xy( x_grid, y_grid )

  type = FILE
  allocate( tab( params_direct%Nx, params_direct%Ny, params_direct%Nz ) )
  call set_signal( type, tab, x_grid, y_grid, "rho2d" )



end program test_fichiers

