! Created by me on 20/09/2021.
! http://lyoncalcul.univ-lyon1.fr/ed/DOCS_2019/dft.pdf

program fftw_1d
  ! FFTW
  use FFTW3
  use mod_signaux

  implicit none

  ! define Pi
  real ( dp ) , parameter :: PI = 4.0_dp * atan (1.0_dp )
  integer :: i , N
  complex ( C_DOUBLE_COMPLEX ) , dimension (:) , allocatable :: ain
  complex ( C_DOUBLE_COMPLEX ) , dimension (:) , allocatable :: aout , aini
  real ( dp ) , dimension (:) , allocatable :: x , kx
  type ( C_PTR ) :: plan , plani
  real ( C_DOUBLE ) :: fact
  ! input data
  N = 16384
  ! allocate
  allocate ( x (0: N -1) )
  allocate ( kx (0: N -1) )
  allocate ( ain (0: N -1) )
  allocate ( aini (0: N -1) )
  allocate ( aout (0: N -1) )

  ! make plan forward and backward
  ain = 1
  aout = 1
  aini = 1
  plan = fftw_plan_dft_1d (N , ain , aout , FFTW_FORWARD , FFTW_ESTIMATE )
  plani = fftw_plan_dft_1d (N , aout , aini , FFTW_BACKWARD , FFTW_ESTIMATE )
  ! normalization
  fact = 1.0_dp / N
  ! case
  call function_periodic_cos_sin (N ,x , kx , ain )
  write (* ,*) " Function in physical space and frequencies "
  do i = 0 ,N -1
     write (10 , '( I5 , 4(1 X , E25 .16) )') i , x ( i ) , ain ( i ) , kx ( i )
  end do

  ! execute forward dft
  call fftw_execute_dft ( plan , ain , aout )
  write (* ,*) " Fourier coefficient after forward FFT "
  do i = 0 ,N -1
    if (real(aout ( i )) > 1e-2) then
      print *, i , kx ( i ) , real(aout ( i )) * fact
    end if
     write (11 , '( I5 , 3(1 X , E25 .16) )') i , kx ( i ) , real(aout ( i )) * fact
  end do
  ! inverse transform
  call fftw_execute_dft ( plani , aout , aini )
  write (* ,*) " Function after forward - backward FFT "
  do i = 0 ,N -1
     write (12 , '( I5 , 5(1 X , E25 .16) )') i , x ( i ) , ain ( i ) , aini ( i ) * fact
  end do
  ! a tracer avec gnuplot:
  ! plot 'runs/runs/tests/fort.12' using 2:3 w dots title "theorique", '' using 2:5 w dots title 'Function after forward - backward FFT'
  print *, "pour plotter le resultat, dans gnuplot: "
  print *, "plot 'runs/fftw_1d/fort.12' using 2:3 w dots title 'theorique'  &
           & , '' using 2:5 w dots title 'Function after forward - backward FFT'"

  ! destroy plans
  call fftw_destroy_plan ( plan )
  call fftw_destroy_plan ( plani )
  ! deallocate
  deallocate ( x )
  deallocate ( ain )
  deallocate ( aini )
  deallocate ( aout )
end program fftw_1d
