!  convoluition 2d à base de fft et fft inverse
! à comparer au code matlab conv2d.m, on doit obtenir les memes valeurs
!
program conv_2d
  use FFTW3

  implicit none

  integer, parameter :: nxm=64, nym=64, nxfft = 2*nxm, nyfft=2*nym
  integer :: ix, iy, i, j

  !  f et g sont les fonctions reelles (input); et h est aussi reele
  !  (output). h = f conv g.
  !
  double precision :: f(nxm,nym), g(nxm, nym), h(nxm, nym)

  !  des parametres juste pour l'exemple
  !
  integer, parameter :: dp = selected_real_kind(15,50)
  complex(dp), parameter :: ucomp=(1.0d0, 0.0d0)
  complex(dp), parameter :: icomp=(0.0d0, 1.0d0)

  ! tableaux de travail pour les fft de taille double
  !
  complex(dp)   :: f2(nxfft,nyfft), g2(nxfft,nyfft), h2(nxfft,nyfft)
  complex(dp)   :: zf2(nxfft,nyfft), zg2(nxfft,nyfft), zh2(nxfft,nyfft)

  integer(8)       :: plan2f, plan2b
  integer          :: iret, omp_get_max_threads ! pour FFT en

  !  DEBUT
  ! ------
  !  determine le nb de procs pour la parallelisation et
  !  initialisation des plans pour les FFT. planf pour FORWARD et
  !  planb pour BACKWARD.

  !  RMQ: ces initialisations devront etre faites

  !  dans le programme appelant
  !
  call dfftw_init_threads(iret)
  call dfftw_plan_with_nthreads(omp_get_max_threads())

  !
  print*,'debut initialisation des plans'
  call dfftw_plan_dft_2d(plan2b, nxfft, nyfft, f2, zf2,&
       & fftw_backward, fftw_estimate)

  call dfftw_plan_dft_2d(plan2f,nxfft, nyfft, f2, zf2,&
       & fftw_forward, fftw_estimate)
  print*,'fin initialisation des plans'

  ! on définit deux portes de largeur D
  f2(:,:) = 0.0d0
  g2(:,:) = 0.0d0

  f2(62:66,62:66) = 1.0d0
  g2(62:66,62:66) = 1.0d0

  open(unit =11, file = 'gauss_f_2d.mat')
  open(unit =12, file = 'gauss_g_2d.mat')
  do iy=1,nyfft
     do ix=1,nxfft
         write(11,*) ix, iy, real(f2(ix,iy)), imag(f2(ix,iy))
         write(12,*) ix, iy, real(g2(ix,iy)), imag(g2(ix,iy))
     enddo
  enddo
  close(11)
  close(12)
  print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
  print *, __FILE__, __LINE__, 'splot &
          & "runs/tests/conv2d/gauss_f_2d.mat" using 1:2:3'

  !  calcul des fft de f et g
  !
  call dfftw_execute_dft(plan2f, f2, zf2)
  call dfftw_execute_dft(plan2f, g2, zg2)
  ! sauvegarde des fft de f et g
  open(unit =11, file = 'fft_f_2d2.mat')
  open(unit =12, file = 'fft_g_2d2.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) ix, iy, real(zf2(ix,iy)), imag(zf2(ix,iy))
        write(12,*) ix, iy, real(zg2(ix,iy)), imag(zg2(ix,iy))
     enddo
  enddo
  close(11)
  close(12)

  !  produit des fft
  !
  zh2 = zf2 * zg2
  open(unit=11, file ='product_fft.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) ix, iy, real(zh2(ix,iy)), aimag(zh2(ix,iy))
     enddo
  enddo
  close(11)

  !  calcul de la fft inverse du produit (convolution dans l'espace
  !  direct)
  print*,'calcul de la fft inverse du produit'
  call dfftw_execute_dft(plan2b, zh2, h2)

  zh2=h2/dble(nxfft*nyfft)

  open(unit=11, file ='raw_h2.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) ix, iy, real(zh2(ix,iy)), aimag(zh2(ix,iy))
     enddo
  enddo
  close(11)


  !  on periodise a nouveau pour remettre dans l'ordre
  !

  do ix=1,nxfft/2
     do iy=1,nyfft/2
        h2(ix,iy)=zh2(nxfft/2+ix,nyfft/2+iy)

        h2(nxfft-ix+1,nyfft-iy+1)=zh2(nxfft/2-ix+1,nyfft/2-iy+1)
        h2(ix,nyfft-iy+1)=zh2(nxfft/2+ix,nyfft/2-iy+1)
        h2(nxfft-ix+1,iy)=zh2(nxfft/2-ix+1,nyfft/2+iy)
     enddo
  enddo
  
  
  open(unit=11, file ='conv2d.mat')
  do iy=1,nyfft
     do ix=1,nxfft
        write(11,*) ix, iy, real(h2(ix,iy)), aimag(zf2(ix,iy))
     enddo
  enddo
  close(11)
  print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
  print *, __FILE__, __LINE__, 'splot &
          & "runs/tests/conv2d/conv2d.mat" using 1:2:3'

  ! on libere les plans
  !
  call dfftw_destroy_plan(plan2f)
  call dfftw_destroy_plan(plan2b)


  !  FIN
  ! ------
end program conv_2d
