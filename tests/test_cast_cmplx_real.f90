program test_cast_cmplx_real
  use, intrinsic :: iso_c_binding
  implicit none

! on teste que le compilateur géénèr bien un message si on affecte un complex à un real
  ! avec ifort et -w sur tramel
  ! ifort  test_cast_cmplx_real.f90
  ! ifort  -warn all test_cast_cmplx_real.f90
  ! avec gfortran auus
  ! gfortran -Wall test_cast_cmplx_real.f90
  
  complex :: z1, z2
  real :: r1, r2
  integer ::  i
  
  r1 = 0.
  r2 = 0.
  z1 = (1, 2)
  z2 = (1.025, 0.25)
  
  do i= 1, 10000
    r1 = r1 + z1
    r2 = z2 + r2
  end do

  print *, "z1 ", z1
  print *, "z2 ", z2
  print *, "r1 ", r1
  print *, "r2 ", r2

end program test_cast_cmplx_real

