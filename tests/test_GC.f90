program aeffa
  use mod_GC_fluo

  implicit none
  
  
  double precision, allocatable :: tableau(:,:,:)
  double precision, dimension(:,:, :), allocatable :: rho_n, rho_n1
  double precision, dimension(:,:, :), allocatable :: grho_n, grho_n1
  double precision, dimension(:,:, :), allocatable :: drho_n1
  double precision, dimension(:,:, :), allocatable :: drho_n
  integer :: nx, ny, nz
  character( len=2) :: type_GC


  type_GC = 'PR'
  nx = 16
  ny = 16
  nz = 2
  print *, " debut: nx ny nz ", nx, ny, nz

  allocate( rho_n( nx, ny, nz ) )
  rho_n = 1.d0
  allocate( rho_n1( nx, ny, nz ) )
  rho_n1 = 1.d0
  allocate( grho_n( nx, ny, nz ) )
  grho_n = 3.d0
  allocate( grho_n1( nx, ny, nz ) )
  grho_n1 = 4.d0
  allocate( drho_n1( nx, ny, nz ) )
  allocate( drho_n( nx, ny, nz ) )

  print *, " avant appel: nx ny nz par size() ", size(rho_n,1), size(rho_n,2), size(rho_n,3)

  call GC_fluo( type_GC, rho_n, rho_n1, grho_n, grho_n1, drho_n1, drho_n )

  print *, " apres appel: nx ny nz par size() ", size(rho_n,1), size(rho_n,2), size(rho_n,3)

  print *, " drho_n1 ", drho_n1
  print *, " drho_n ", drho_n

contains
  subroutine bidon(tableau)
  implicit none
  double precision, dimension(:,:,:), intent(in) :: tableau
  integer nx, ny,nz

  nx=size(tableau,1)
  ny=size(tableau,2)
  nz=size(tableau,3)
  print*, nx, ny ,nz
end subroutine bidon

end program aeffa
