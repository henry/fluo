!! code K. Belkebir
! 27 sept 21
! but: calculer une fft 1d
! comprendre les manipulations de tableau
! et les différentes discrétisation
! il faut partir d'un pas fixé plutôt que d'un nombre d'échantillons
! 29 set 21
! modif GH pour prendre les mêmes paramètres que le script matlab de J. Wojak
! https://gitlab.fresnel.fr/henry/fluo/-/blob/master/matlab/fft_demo.m

program fff1D
  use, intrinsic :: iso_c_binding
  use FFTW3

  implicit none

  integer :: n, i, ns2, nfft
  double precision, allocatable, dimension(:) :: f
  complex ( C_DOUBLE_COMPLEX ), allocatable, dimension(:) ::f2, fft2
  double precision e, dx, lx, lxnfft
  double precision, allocatable, dimension(:) :: x, xfft, FreqAxis
  type ( C_PTR ) :: plan , plani
  complex ( C_DOUBLE_COMPLEX ) ucomp
  
  ! domaine
  double precision :: xmin
  
  ! fonction analytique
  double precision , dimension (:) , allocatable :: f_analy

  double precision , parameter :: PI = 4.0d0 * atan (1.0d0 )

  ucomp = cmplx(1.0D0,0.0D0)
  ! on se fixe le pas, quel que soit le nbre d'echantillons (parce que la résolution ne dépendant pas de la
  ! frequence d'echanbtillonnage mais seulement de la duree d'observation (lx)
  ! une valeur coherente avec la fonction a echantillonner
  ! ! en partant d'un N faible et qu'on va faire varier en l'augmentant

  dx = 12.0d0/16.0d0  ! fs dans le script matlab
  dx = 12.0d0/1024.0d0  ! fs dans le script matlab
  dx = 12.0d0/128.0d0  ! fs dans le script matlab
  n  = 16
  n  = 1024
  n  = 128
  !!! nfft >= n !!!
  nfft = 1024
  nfft   = 32
  nfft = 2048
  nfft = 128
  print *, "nb de points de la fft ", nfft

  lx = dble(n)*dx

  e = dble(n/4)*dx! JW appelle ça A
  print *, "largeur de la porte ", e

  xmin = -lx/2
  print *, "largeur du signal ", lx
  
!  dx = lx / dble( n - 1) ! idem Julien fs
  print *, "pas de discretisation ", dx
  print *, "nombre de points ", n
  ns2 = n/2  ! n sur deux
  
  !lx = dble(n-1)*dx
  print *, "largeur du signal ", lx

  lxnfft = dble(nfft)*dx ! on prend le meme pas qu'en x meme si c'est le
  print *, "taille du domaine de la fft: (nfft)*dx   ", lxnfft

  allocate(f(n))
  allocate(x(n))

  print *, __FILE__, __LINE__, 'discretisation en x d une fonction' ! JW appelle ça t[]
  print *, '#        i                 x(i)'
  do i=1,n
    x(i) = -0.5d0*lx + dble(i-1)*dx
    print*, i, x(i)
  enddo
  print *, __FILE__, __LINE__,  'et on verifie que le 0 est bien au milieu'

  print *
  print *, '# i           x(i)                 f(i)'
  do i=1,n
    f(i) = porte(x(i),e)
    print*, i, x(i), f(i)
  enddo
  !f(ind_debut_porte(f)) = 0.0d0

  allocate(fft2(nfft))
  allocate(f2(nfft)) ! c'est un tableau utilise pour stocker la porte et qui sert pour le calcul de la fft
  allocate(xfft(nfft))
  allocate(FreqAxis(nfft))

  print *, __FILE__, __LINE__, "tableau xfft pour le zero-padding et la periodisation"

  print *, __FILE__, __LINE__, 'discretisation en x de la fonction avec le zero padding'
  print *, '# i    xfft(i)'
  do i=1,nfft
    xfft(i) = -0.5d0*lxnfft + dble(i-1)*dx
    print*, i, xfft(i)
  enddo
  print *, __FILE__, __LINE__, 'et on verifie que le 0 est bien au milieu'
  
  plan = fftw_plan_dft_1d (Nfft , f2 , fft2 , FFTW_FORWARD ,&
       & FFTW_ESTIMATE )
  plani = fftw_plan_dft_1d (Nfft , fft2 , f2 , FFTW_BACKWARD ,&
       & FFTW_ESTIMATE )

  ! on remplit le nouveau tableau plus grand, avec du zero-padding
  print *, "# i                 xfft                                              f2"
  do i=1,nfft
     f2(i) = ucomp*porte(xfft(i), e)
     print*, i, xfft(i), f2(i)
  enddo

  print *, __FILE__, __LINE__, 'on periodise la nouvelle fonction (avec cshift)'
  print *, '# i                 xfft(i)                   f2n(i)'
  f2 = cshift(f2, nfft/2, 1)
  do i=1, nfft
    print*, i, xfft(i), f2(i)
  enddo
  print *
  
  !f2 = f
  print *, __FILE__, __LINE__, 'on calcule la FFT'
  call fftw_execute_dft ( plan , f2 , fft2 )
  ! on remet les frequences dans l'ordre
  fft2 = cshift(real(fft2), nfft/2, 1)
  
  ! calcul de     FreqAxis du psectre
  do i = -nfft/2, (nfft/2)-1
    FreqAxis(i+(nfft/2)+1) = i * ((1/dx)/nfft)
  end do

  allocate ( f_analy (n) )
  do i = 1 ,n
     if ( x(i) /= 0.0d0 ) then
        f_analy(i) = e*sin(PI*x(i)*e)/(PI*x(i)*e)
     else
        f_analy(i) = e
     end if
  end do
  open(unit=11, file= 'fftanaly')
  write(11,*) '#      i      x(i)    f_analy(i)  '
  do i=1,n
    write(11,*) i, x(i), f_analy(i)
  enddo
  close(11)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!! Enregistrement des resultats !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  print*, __FILE__, __LINE__, 'on ecrit les donnees dans le fichier'
  open(unit=11, file= 'affa')
  write(11,*) '#12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789&
        &0123456789012345678901234567890123456789012345678901234567890'
  write(11,*) '#         i      xfft(i)                   FreqAxis(i)                  real(f2(i))                 &
          & abs(fft2(i))             real(fft2(i))           aimag(fft2(i))' !             x(i)                f_analy(i)  '
  do i=1,nfft
    write(11,*) i, xfft(i), FreqAxis(i), real(f2(i)), abs(fft2(i)), real(fft2(i))*dx, aimag(fft2(i))
  enddo
  close(11)
  print *, __FILE__, __LINE__, 'avec gnuplot, utiliser la commande'
  print *, __FILE__, __LINE__, 'plot "affa" using 3:6 w l title "fftw3", &
          & "fftanaly"  using 2:3 w l title "analy"'

  deallocate(f)
  deallocate(x)

contains
  function porte(x,e)
    double precision porte
    double precision x, e
    double precision tmp

    if (abs(x) <= 0.5D0*e) then
       tmp = 1.0D0
    else
       tmp = 0.0D0
    endif
  end function porte
  
  end program fff1D

