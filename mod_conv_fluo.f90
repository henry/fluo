!  Ce code calcule l'image de fluorescence a partir de la densite de
!  fluorophores, l'intensite dans le domaine des fluorophores et la
!  psf du microscope. La convolution est calcule a l'aide des FFT pour
!  les fonctions reelles. Les FFT sont dans ce cas hermitiennes.
!
! Kamal BELKEBIR (novembre 2019)
!
module  mod_conv_fluo

  use mod_fft_param

  implicit none

contains

  function conv_2D(f, g, nxm, nym, kx, ky) result(h2)
    implicit none
    double precision, dimension(nxm,nym), intent(in) :: f, g
    integer, intent(in)  :: nxm, nym
    double precision, dimension(:), intent(inout) :: kx
    double precision, dimension(:), intent(inout) :: ky
    double complex, dimension(nxfft,nyfft) :: h2

    integer :: nx, ny, ix, iy, i, j

    !  f et g sont les fonctions reelles (input); et h est aussi reele
    !  (output). h = f conv g.
    !

    !  des parametres juste pour l'exemple
    !
    double precision :: sigma, pi, twopi, x(nxm), y(nym), lx, ly, l
    double precision, dimension(nxfft) :: x_double
    double precision, dimension(nyfft) :: y_double
    double precision :: l2, deltax2
    double precision :: x0, y0
    double precision :: deltax, deltay
    double precision :: deltakx, deltaky

    ! tableaux de travail pour les fft de taille double
    !
    double complex, dimension(nxfft,nyfft) :: zf2, zg2, zh2
    double complex, dimension(nxfft,nyfft) :: f2, g2

    integer(8)       :: plan2f, plan2b
    integer          :: iret, omp_get_max_threads ! pour FFT en
    ! parallelisation
    integer          :: fftw_forward, fftw_estimate, fftw_backward

    !  DEBUT
    ! ------
    !  determine le nb de procs pour la parallelisation et
    !  initialisation des plans pour les FFT. planf pour FORWARD et
    !  planb pour BACKWARD.

    !  RMQ: ces initialisations devront etre faites

    !  dans le programme appelant
    !
    call dfftw_init_threads(iret)
    call dfftw_plan_with_nthreads(omp_get_max_threads())

    !  ces parametres sont consistants avec fluotomofwd
    !
    fftw_forward =-1 !  definit la FFT direct
    fftw_backward=+1 !  definit la FFT inverse
    fftw_estimate=64 !  choix de l'algorithme pour les plans

    !
#ifdef DEBUG
    print *, __FILE__, __LINE__, "debut initialisation des plans"
#endif
    call dfftw_plan_dft_2d(plan2b, nxfft, nyfft, f2, zf2,&
         & fftw_backward, fftw_estimate)

    call dfftw_plan_dft_2d(plan2f,nxfft, nyfft, f2, zf2,&
         & fftw_forward, fftw_estimate)
#ifdef DEBUG
    print *, __FILE__, __LINE__, "fin initialisation des plans"
#endif

    nx=64
    ny=64

    pi=acos(-1.0d0)
    twopi=2.0d0*pi
    sigma=1.0d0
    x0=0.0d0
    y0=0.0d0

    ! echantillonage dans l'espace direct
    !
    l=10.0d0
    lx=l
    ly=l
    deltax=lx/dble(nx)
    deltay=ly/dble(ny)

    ! RMQ : le centre est en (nx/2+1, ny/2+1)
    !
#ifdef DEBUG
    print*, __FILE__, __LINE__,'calcul des points echantillonage dans l espace direct'
#endif
    do ix = 1,nx
       x(ix)=-lx/2.0d0+dble(ix-1)*deltax
    enddo
    do iy = 1,ny
       y(iy)=-ly/2.0d0+dble(iy-1)*deltay
    enddo



    !  echantillonage dans l'espace de FOURIER RMQ : la frequence
    !  spatiale nulle est en nfft/2+1. Les librairies permettant de
    !  calculer les FFT considèrent des supports positifs. Faut tenir
    !  compte de cela.
    !
    deltakx=twopi/(deltax*dble(nxfft))
    deltaky=twopi/(deltay*dble(nyfft))
    do ix=-nxfft/2, nxfft/2-1
       i=ix+nxfft/2+1
       kx(i)=dble(ix)*deltakx
    enddo
    do iy=-nyfft/2, nyfft/2-1
       j=iy+nyfft/2+1
       ky(j)=dble(iy)*deltaky
    enddo

    !  pour la convolution on calcule les tf des deux fonctions, on les
    !  multiplie dans FOURIER puis en revient dans l'espace
    !  direct. Pour le calcul des tf on va suivre la meme demarche
    !  qu'en 1D. Le reste est trivial !

    !  on sauvegarde les signaux originaux de l'espace direct. Deja
    !  calcules plus haut
    !
    open(unit =11, file = 'gauss_f_2d.mat')
    open(unit =12, file = 'gauss_g_2d.mat')
    do iy=1,ny
       do ix=1,nx
          write(11,*) f(ix,iy)
          write(12,*) g(ix,iy)
       enddo
    enddo
    close(11)
    close(12)


    !  on double les tableaux en periodisant et avec un zero-padding
    !

    do ix=1,nxfft
       do iy=1,nyfft
          f2(ix,iy)=0.0d0  !  initialise f2 
          g2(ix,iy)=0.0d0  !  initialise g2
       enddo
    enddo


    do ix =1, nx/2
       do iy=1, ny/2
          !  la fonction f
          !
          f2(ix,iy)=f(nx/2+ix,ny/2+iy)
          f2(nxfft-ix+1,nyfft-iy+1)=f(nx/2-ix+1,ny/2-iy+1)
          f2(ix,nyfft-iy+1)=f(nx/2+ix,ny/2-iy+1)
          f2(nxfft-ix+1,iy)=f(nx/2-ix+1,ny/2+iy)

          !  la fonction g
          !
          g2(ix,iy)=g(nx/2+ix,ny/2+iy)
          g2(nxfft-ix+1,nyfft-iy+1)=g(nx/2-ix+1,ny/2-iy+1)
          g2(ix,nyfft-iy+1)=g(nx/2+ix,ny/2-iy+1)
          g2(nxfft-ix+1,iy)=g(nx/2-ix+1,ny/2+iy)
       enddo
    enddo

    !  sauvegarde des fonctions periodisees
    !
    l2=2.0*l
    deltax2=2.0*l2/dble(nxfft)
    do ix=1,nxfft
       x_double(ix)=-l+dble(ix-1)*deltax2
    enddo
    do iy=1,nyfft
       y_double(iy)=-l+dble(iy-1)*deltax2
    enddo
    open(unit =11, file = 'gauss_f_2d2.mat')
    open(unit =12, file = 'gauss_g_2d2.mat')
    do iy=1,nyfft
       do ix=1,nxfft
          write(11,*) x_double(ix), y_double(iy), real(f2(ix,iy))
          write(11,*) x_double(ix), y_double(iy), real(g2(ix,iy))
       enddo
    enddo
    close(11)

    !  calcul des fft de f et g
    !
#ifdef DEBUG
    print *, __FILE__, __LINE__, "debut de la convolution"
#endif
    call dfftw_execute_dft(plan2f, f2, zf2)
    call dfftw_execute_dft(plan2f, g2, zg2)

    !  produit des fft
    !
    do ix=1,nxfft
       do iy=1,nyfft
          zh2(ix,iy)= zf2(ix,iy)*zg2(ix,iy)
       enddo
    enddo

    !  calcul de la fft inverse du produit (convolution dans l'espace
    !  direct)

#ifdef DEBUG
    print *, __FILE__, __LINE__, "Calcul de FFT de f*g termine"
#endif
    call dfftw_execute_dft(plan2b, zh2, h2)

    zh2=h2/dble(nxfft*nyfft)

    !  on periodise a nouveau pour remettre dans l'ordre
    !

    do ix=1,nxfft/2
       do iy=1,nyfft/2
          h2(ix,iy)=zh2(nxfft/2+ix,nyfft/2+iy)

          h2(nxfft-ix+1,nyfft-iy+1)=zh2(nxfft/2-ix+1,nyfft/2-iy+1)
          h2(ix,nyfft-iy+1)=zh2(nxfft/2+ix,nyfft/2-iy+1)
          h2(nxfft-ix+1,iy)=zh2(nxfft/2-ix+1,nyfft/2+iy)

       enddo
    enddo
    open(unit=11, file ='real_conv2d.mat')
    open(unit=12, file ='imag_conv2d.mat')
    do iy=1,nyfft
       do ix=1,nxfft
          write(11,*) x_double(ix), y_double(iy), real(h2(ix,iy))
          write(12,*) x_double(ix), y_double(iy), aimag(zf2(ix,iy))
       enddo
    enddo
    close(11)
    close(12)



    ! on libere les plans
    !
    call dfftw_destroy_plan(plan2f)
    call dfftw_destroy_plan(plan2b)


    !  FIN
    ! ------
  end function conv_2D
end module mod_conv_fluo
