! https://cpinettes.u-cergy.fr/S6-Electromag_files/Fresnel.pdf
! Programme crée par Kamal Belkebir ce 1er mars
! le code met en oeuvre le calcul des coefficients de Fresnel
! dans deux milieux 1 et 2
! la longueur d'onde est fixée au début du programme
program coeff_fresnel
    use, intrinsic :: iso_c_binding
    use mod_coeff_fresnel
    use mod_physi, only : pi, ucomp
    
    implicit none
    integer :: i
    double precision :: theta, thetad
    double complex :: zrpara, zrortho
    double complex :: ztpara, ztortho
    double complex :: beta1, beta2, ztemp
    double complex :: zn1, zn2, zk1, zk2
    double precision :: lambda
    parameter (lambda = 500.0d-9) ! longueur d'onde
    double precision :: k0
    parameter (k0 = (2.0D0 * pi / lambda))  ! nombre d'onde dans le vide

    parameter (zn1 = 1.0D0 * ucomp)        ! air
    parameter (zn2 = sqrt(2.25d0) * ucomp) ! verre
    !
    parameter (zk1 = k0 * zn1)   ! nombre d'onde dans le milieu n1
    parameter (zk2 = k0 * zn2)   ! nombre d'onde dans le milieu n2
    
    character (len = *), parameter :: file_refl_para = 'refl_para.dat'
    character (len = *), parameter :: file_tran_para = 'tran_para.dat'
    character (len = *), parameter :: file_refl_ortho = 'refl_ortho.dat'
    character (len = *), parameter :: file_tran_ortho = 'tran_ortho.dat'
    
    if (real(zn1) > real(zn2)) then
        write(*, *) " TODO revoir le calcul de beta_i"
        stop 1
    else
        print *, zn1, " air"
        print *, zn2, " verre"
        ztemp = zn2 / zn1
        print *, "n2/n1 ", ztemp
        !theta = atan2(aimag(ztemp), real(ztemp))
        theta = atan(ztemp) !! parce que la partie imaginiare est nulle
        thetad = theta * 180 / PI
        print *, "Angle de Brewster ", theta, thetad
        print *, "n2*cos(thetaB) ", zn2 * cos(theta)
        print *, "n1*sin(thetaB) ", zn1 * sin(theta)
    end if
    
    open(unit = 11, file = file_refl_para)
    open(unit = 12, file = file_tran_para)
    open(unit = 13, file = file_refl_ortho)
    open(unit = 14, file = file_tran_ortho)
    
    do i = 0, 1800
        thetad = dble(i) * 0.05D0
        theta = thetad * pi / 180.0D0
        
        call calcul_coeff_reflexion(theta, &
                & zk1, zk2, &
                & zn1, zn2, &
                & zrpara, ztpara, zrortho, ztortho, &
                & beta1, beta2)
        ztemp = beta2 / beta1
        write(11, *) thetad, abs(zrpara)**2, real(zrpara), imag(zrpara)
        write(12, *) thetad, abs(ztemp) * abs(ztpara)**2, real(ztpara), imag(ztpara)
        ztemp = (beta2 / (zn2**2)) / beta1
        write(13, *) thetad, abs(zrortho)**2, real(zrortho), imag(zrortho)
        write(14, *) thetad, abs(ztemp) * abs(ztortho)**2, real(ztortho), imag(ztortho)
    
    enddo
    
    close(11)
    print *, "sauvegarde résultats réflexion parallèle dans ", file_refl_para
    close(12)
    print *, "sauvegarde résultats transmission parallèle dans ", file_tran_para
    close(13)
    print *, "sauvegarde résultats réflexion orthogonale dans ", file_refl_ortho
    close(14)
    print *, "sauvegarde résultats transmission orthogonale dans ", file_tran_ortho

end program coeff_fresnel
