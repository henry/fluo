! Created by me on 29/11/2021.

module mod_aide

  implicit none

contains
  subroutine affiche_aide()
    character(len=*), parameter :: string1="il faut un fichier contenant les mesures\n &
         &la valeur de l'illumination\n &
         & la valeur de la PSF\n"
    print *, ""
    print *, "il faut un fichier contenant les mesures"
    print *, "la valeur de l'illumination"
    print *, "la valeur de la PSF"
    print *, "tous ces parametres se reglent dans le fichier &
         &fluo_inverse.IN, peu importe le nom"
    print *, ""
  end subroutine affiche_aide

end module mod_aide
